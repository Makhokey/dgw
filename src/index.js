import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Routes from './routes'
import SidebarProvider from './providers/Sidebar'
import CoordinatesProvider from './providers/GooglePlaces'
import SearchProvider from './providers/Search'
import AgeGate from './providers/AgeGate'
import SelectedDistrict from './providers/SelectedDistrict'
import HeaderTitle from './providers/HeaderTitle'

// eslint-disable-next-line
ReactDOM.render(
  <SidebarProvider>
    <SearchProvider>
      <CoordinatesProvider>
        <AgeGate>
          <SelectedDistrict>
            <HeaderTitle>
              <Routes />
            </HeaderTitle>
          </SelectedDistrict>
        </AgeGate>
      </CoordinatesProvider>
    </SearchProvider>
  </SidebarProvider>,
  document.getElementById('root')
)
