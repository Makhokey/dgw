const sizes = {
  // padding
  tinyPadding: '10px',
  smallPadding: '7px',
  largePadding: '12px',
  extraLargePadding: '15px',

  // margin
  tinyMargin: '10px',
  smallMargin: '20px',
  normal: '20px',
  medium: '22px',
  largeMargin: '40px',
  extraLargeMargin: '25px',

  // letterSpacing
  tinySpacing: '0.6px',
  smallSpacing: '1px',
  normalSpacing: '1.2px',
  mediumSpacing: '',
  largeSpacing: '',
  extraLargeSpacing: '',

  // line height
  tinyLineHeight: '20px',
  smallLineHeight: '23px',
  normalLineHeight: '25px',

  //elements
  header: '60px',

  //width
  defaultWidth: '100%',

  // media
  media: {
    giant: '1170px',
    desktop: '769px',
    tablet: '768px',
    phone: '499px',
    low: '350px',
    tiny: '220px'
  }
}

export default sizes

export const giant = `@media (min-width: ${sizes.media.desktop})`
export const desktop = `@media (max-width: ${sizes.media.giant}) and (min-width: ${sizes.media.desktop})`
export const tablet = `@media (max-width: ${sizes.media.tablet}) and (min-width: ${sizes.media.phone})`
export const phone = `@media (max-width: ${sizes.media.phone})`
export const tiny = `@media (max-width: ${sizes.media.tiny})`
export const low = `@media (max-height: ${sizes.media.low})`
