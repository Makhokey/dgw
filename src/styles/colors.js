const colors = {
  darkBlue: 'rgb(2, 52, 102)',
  lightBlue: 'rgb(127, 165, 208)',
  gray: 'rgb(76, 76, 76)',
  lightGray: 'rgb(204,204,204)',
  bgWhite: 'white',
  white: 'white',
  bgBlue: '#000a32'
}

export default colors
