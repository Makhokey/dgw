import TweenMax from 'gsap'

export const toggleEntries = id => {
  TweenMax.to(`#${id}`, 1, { opacity: 0 })
}

export const pushDown = id => {
  TweenMax.to(`#${id}`, 1, { marginTop: '155px' })
}
