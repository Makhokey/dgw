import styled, { injectGlobal } from 'styled-components'
import colors from './colors'
import fonts from './fonts'
import sizes from './sizes'
import PernodBold from '../assets/fonts/pernodricard_bold-webfont.woff'
import PernodBoldItalic from '../assets/fonts/pernodricard_bolditalic-webfont.woff'
import PernodItalic from '../assets/fonts/pernodricard_italic-webfont.woff'
import PernodRegular from '../assets/fonts/pernodricard_regular-webfont.woff'
import RobotoBold from '../assets/fonts/roboto-bold-webfont.woff'
import RobotoItalic from '../assets/fonts/roboto-italic-webfont.woff'
import RobotoLight from '../assets/fonts/roboto-light-webfont.woff'
import RobotoRegular from '../assets/fonts/roboto-regular-webfont.woff'
import RobotoMedium from '../assets/fonts/roboto-medium-webfont.woff'
import PernodBoldWoff2 from '../assets/fonts/pernodricard_bold-webfont.woff2'
import PernodBoldItalicWoff2 from '../assets/fonts/pernodricard_bolditalic-webfont.woff2'
import PernodItalicWoff2 from '../assets/fonts/pernodricard_italic-webfont.woff2'
import PernodRegularWoff2 from '../assets/fonts/pernodricard_regular-webfont.woff2'
import RobotoBoldWoff2 from '../assets/fonts/roboto-bold-webfont.woff2'
import RobotoItalicWoff2 from '../assets/fonts/roboto-italic-webfont.woff2'
import RobotoLightWoff2 from '../assets/fonts/roboto-light-webfont.woff2'
import RobotoRegularWoff2 from '../assets/fonts/roboto-regular-webfont.woff2'
import RobotoMediumWoff2 from '../assets/fonts/roboto-medium-webfont.woff2'

injectGlobal`
  @font-face {
    font-family: PernodBold;
    src: url('${PernodBoldWoff2}') format('woff2'),
        url('${PernodBold}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: PernodItalic;
    src: url('${PernodItalicWoff2}') format('woff2'),
         url('${PernodItalic}') format('woff');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: PernodRegular;
    src: url('${PernodRegularWoff2}') format('woff2'),
         url('${PernodRegular}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: PernodBoldItalic;
    src: url('${PernodBoldItalicWoff2}') format('woff2'),
         url('${PernodBoldItalic}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: RobotoRegular;
    src: url('${RobotoRegularWoff2}') format('woff2'),
         url('${RobotoRegular}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: RobotoLight;
    src: url('${RobotoLightWoff2}') format('woff2'),
         url('${RobotoLight}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: RobotoItalic;
    src: url('${RobotoItalicWoff2}') format('woff2'),
         url('${RobotoItalic}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: RobotoBold;
    src: url('${RobotoBoldWoff2}') format('woff2'),
         url('${RobotoBold}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: RobotoMedium;
    src: url('${RobotoMediumWoff2}') format('woff2'),
         url('${RobotoMedium}') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  body {
    font-size: ${fonts.sizes.h2};
    margin: 0;
    padding: 0;
    height: 100vh;
    box-sizing: border-box;
    /* max-width: 700px; */
  }

  h1 {
    font-family: RobotoRegular;
  }

  link {
    text-decoration: none;
  }
`

// display: flex;
// flex-direction: column;
// align-items: flex-end;
// margin-right: 10%;
