const fonts = {
  types: {
    main: 'Sans-Serif, Halvetica',
    secondary: 'Sans-Serif, Halvetica'
  },

  sizes: {
    tiny: '10px',
    small: '12px',
    normal: '14px',
    medium: '15px',
    large: '17px',
    extraLarge: '20px'
  },

  weights: {
    bold: 600,
    normal: 400,
    light: 300
  }
}

export default fonts
