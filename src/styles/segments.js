import styled from 'styled-components'
import colors from './colors'
import fonts from './fonts'
import sizes from './sizes'

// ==============================================================================
//    TEXT
// ==============================================================================

const textColorChooser = ({ white, gray, lightBlue, lightGray, darkBlue }) => {
  if (white) return `${colors.white};`
  else if (gray) return `${colors.gray};`
  else if (lightGray) return `${colors.lightGray}`
  else if (lightBlue) return `${colors.lightBlue}`
  else if (darkBlue) return `${colors.darkBlue};`
}

const fontFamilyChooser = ({
  PernodRegular,
  PernodItalic,
  PernodBold,
  PernodItalicBold,
  RobotoRegular,
  RobotoBold,
  RobotoLight,
  RobotoMedium
}) => {
  if (PernodRegular) return `PernodRegular`
  else if (PernodItalic) return `PernodItalic`
  else if (RobotoRegular) return `RobotoRegular`
  else if (RobotoBold) return `RobotoBold`
  else if (RobotoLight) return `RobotoLight`
  else if (RobotoMedium) return `RobotoMedium`
  else if (PernodBold) return `PernodBold`
  else if (PernodItalicBold) return `PernodItalicBold`
}

const fontSizeChooser = ({ tiny, small, normal, medium, large, extraLarge }) => {
  if (tiny) return `${fonts.sizes.tiny}`
  else if (small) return `${fonts.sizes.small}`
  else if (normal) return `${fonts.sizes.normal}`
  else if (medium) return `${fonts.sizes.medium}`
  else if (large) return `${fonts.sizes.large}`
  else if (extraLarge) return `${fonts.sizes.extraLarge};`
}

const fontStyleChooser = ({ italic, bold }) => {
  if (italic) return 'italic'
  else if (bold) return 'bold'
}

const justifyContentChooser = ({ flexEnd, justifyCenter, spaceAround, spaceBetween }) => {
  if (flexEnd) return 'flex-end'
  else if (justifyCenter) return 'center'
  else if (spaceAround) return 'space-around'
  else if (spaceBetween) return 'space-between'
}

const letterSpacingChooser = ({ tinySpacing, smallSpacing, normalSpacing }) => {
  if (tinySpacing) return `${sizes.tinySpacing}`
  else if (smallSpacing) return `${sizes.smallSpacing}`
  else if (normalSpacing) return `${sizes.normalSpacing}`
}

const lineHeightChooser = ({ tinyHeight, smallHeight, normalHeight }) => {
  if (tinyHeight) return `${sizes.tinyLineHeight}`
  else if (smallHeight) return `${sizes.smallLineHeight}`
  else if (normalHeight) return `${sizes.normalLineHeight}`
}

const Text = styled.div`
  font-family: ${fontFamilyChooser};
  font-size: ${fontSizeChooser};
  text-align: ${props => props.center && 'center'};
  display: ${props => props.flex && 'flex'};
  line-height: 25px;
  ${'' /* text-align: center; */}
  flex-direction: ${props => props.column && 'column'};
  justify-content: ${justifyContentChooser};
  flex-wrap: ${props => props.wrap && 'wrap'};
  font-style: ${fontStyleChooser};
  margin-top: ${props => (props.topMargin ? `${sizes.smallMargin}` : '0')};
  margin-bottom: ${props => (props.bottomMargin ? `${sizes.smallMargin}` : '0')};
  letter-spacing: ${letterSpacingChooser};
  color: ${textColorChooser};
  width: ${props => (props.defaultWidth ? `${sizes.defaultWidth}` : 'auto')};
`

export default Text
