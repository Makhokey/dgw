import styled from 'styled-components'
import color from '../../styles/colors'


export const Wrap = styled.div`
  background-color: ${color.bgBlue};
  min-height: 100vh;
`

export const MainWrapper = styled.div`
  margin: auto;
  max-width: 700px;
  height: 100%;
`

export const Wrapper = styled.div`
  height: 100vh;
  padding: 50px 20px 50px;
  background-color: ${color.bgBlue};
  a {
    color: white;
    word-break: break-word;
  }
`