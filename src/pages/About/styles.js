import styled from 'styled-components'
import color from '../../styles/colors'
import Image from '../../assets/images/cardCopy.png'
import Text from '../../styles/segments'
import Background from '../../assets/images/bg.jpg'

export const Wrapper = styled.div`
  height: 100%;
  padding: 50px 20px 0;
  background-color: ${color.bgBlue};
  background-image: ${Background};
  a {
    color: white;
  }
`

export const Wrap = styled.div`
  background-color: ${color.bgBlue};
  position: absolute;
    height: 100%;
    width: 100%;
`

export const MainWrapper = styled.div`
  margin: auto;
  max-width: 700px;
  height: 100%;
`

export const InfoLine = styled.div`
  display: flex;
  align-items: center;
  margin: 12px 0;
  height: 32px;
  color: white;
`
export const SmallUnderLine = styled.div`
  width: 100%;
  border-bottom: 1px solid ${color.lightGray};
`

export const Img = styled.div`
  background-image: url(${Image});
  background-size: cover;
  width: 100%;
  margin: auto;
  height: 200px;
`

export const ArrowWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 60px;
  margin-bottom: 20px;
`

export const TextBox = styled(Text)`
  max-width: 100%;
  height: 100%;
  background-color: ${color.bgBlue};
  border: 2px solid rgb(2, 52, 102);
  padding: 20px;
  box-sizing: border-box;
`

export const Space = styled.div`
  height: 40px;
`
