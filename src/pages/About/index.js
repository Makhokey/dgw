import React, { Component } from 'react'
import ReactSVG from 'react-svg'
import Header from '../../components/Header'
import {
  Wrapper,
  MainWrapper,
  InfoLine,
  SmallUnderLine,
  Img,
  TextBox,
  Wrap,
  ArrowWrapper,
  Space
} from './styles'
import { LocalText, UnderLine } from '../../components/Filter/styles'
import Text from '../../styles/segments'
import LinkIcon from '../../assets/icons/Link-white.svg'
import MailIcon from '../../assets/icons/Mail-white.svg'
import PinIcon from '../../assets/icons/Pin-white.svg'
import arrowDownWhite from '../../assets/icons/arrowDownWhite.svg'
import TelephoneIcon from '../../assets/icons/Telephone-white.svg'
import Map from '../../components/GoogleMaps'
import Sidebar from '../../components/Sidebar'
import { Helmet } from 'react-helmet'
import axios from 'axios'

class About extends Component {
  state = { data: '' }

  componentDidMount() {
    axios.get(`https://drinkguideadmin.azurewebsites.net/about.json`).then(res => {
      this.setState({ data: res.data.data[0] })
    })
  }

  render() {
    let firstDescription = this.state.data.firstDescription
    let firstBodyText = this.state.data.firstBodyText
    let secondDescription = this.state.data.secondDescription
    let secondBodyText = this.state.data.secondBodyText

    return (
      <Wrap>
        <Helmet>
          <title>Contact</title>
        </Helmet>
        <Header about />
        <Sidebar />

        <MainWrapper>
          <Wrapper>
            <LocalText large white PernodRegular>
              {this.state.data.title}
            </LocalText>
            <UnderLine />
            <Text lightBlue PernodItalic>
              {' '}
              <div dangerouslySetInnerHTML={{ __html: firstDescription }} />{' '}
            </Text>

            <Text white RobotoLight style={{ marginTop: '20px' }}>
              {' '}
              <div dangerouslySetInnerHTML={{ __html: firstBodyText }} />{' '}
            </Text>
            <InfoLine>
              <ReactSVG
                path={LinkIcon}
                svgStyle={{ width: 20, marginRight: 13, marginTop: 2 }}
              />
              <Text RobotoLight>
                <a href={this.state.data.aboutWebsite} target="_blank">{this.state.data.aboutWebsite}</a>
              </Text>
            </InfoLine>
            <SmallUnderLine />
            <InfoLine>
              <ReactSVG
                path={MailIcon}
                svgStyle={{ width: 20, marginRight: 13, marginTop: 5 }}
              />
              <Text RobotoLight>
                <a href={`mailto:` + this.state.data.aboutMail}>{this.state.data.aboutMail}</a>
              </Text>
            </InfoLine>
            <SmallUnderLine />
            <InfoLine>
              <ReactSVG
                path={TelephoneIcon}
                svgStyle={{ width: 20, marginRight: 13, marginTop: 2 }}
              />
              <Text RobotoLight>
                <a href={`tel:${this.state.data.aboutNumber}`}>
                  {this.state.data.aboutNumber}
                </a>
              </Text>
            </InfoLine>
            <SmallUnderLine />
            
          </Wrapper>
          
        </MainWrapper>
      </Wrap>
    )
  }
}

export default About
