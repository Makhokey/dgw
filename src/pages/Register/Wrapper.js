import styled from 'styled-components'
import bgImage from '../../assets/images/bottles-2.jpg'

const Wrapper = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  min-width: 100px;
  min-height: 100vh;
  background-image: url(${bgImage});
  display: flex;
  justify-content: center;
`

export default Wrapper
