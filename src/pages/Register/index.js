import React, { Component } from 'react'
import Wrapper from './Wrapper'
import RegisterCard from '../../components/RegisterCard'

import { MyContext } from '../../index'

class Register extends Component {
  render() {
    return (
      <Wrapper>
        <RegisterCard />
      </Wrapper>
    )
  }
}

export default Register
