import React, { Component } from 'react'
import { Wrapper } from './styles'
import Text from '../../styles/segments'
import { Helmet } from 'react-helmet'
import ReactSVG from 'react-svg'
import arrowLeftWhite from '../../assets/icons/arrowLeftWhite.svg'
import { Link } from 'react-router-dom'
import axios from 'axios'

class PrivacyPolicy extends Component {
  state = { data: '' }

  componentDidMount() {
    axios
      .get(`https://drinkguideadmin.azurewebsites.net/privacy-policy.json`)
      .then(res => {
        this.setState({ data: res.data.data[0] })
      })
  }
  render() {
    return (
      <Wrapper>
        <Helmet>
          <title>Privacy Policy</title>
        </Helmet>
        <div style={{ width: 100, marginLeft: -15, marginTop: -8 }}>
          <Link to="/feed">
            <ReactSVG
              path={arrowLeftWhite}
              svgStyle={{ width: 10, marginLeft: '14px' }}
            />
          </Link>
        </div>
        <Text white RobotoRegular>
          <h1>{this.state.data.title}</h1>
          <div dangerouslySetInnerHTML={{ __html: this.state.data.body }} />{' '}
        </Text>
      </Wrapper>
    )
  }
}
export default PrivacyPolicy
