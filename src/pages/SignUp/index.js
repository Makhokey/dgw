import React, { Component } from 'react'
import { Wrapper, InnerWrapper, Text, InputsWrapper } from './styles'
import ReactSVG from 'react-svg'
import Logo from '../../assets/logos/Large.svg'
import { Link } from 'react-router-dom'
import './styles.css'
import { Helmet } from 'react-helmet'

class Email extends Component {
  constructor() {
    super()

    this.passwordInput = this.passwordInput.bind(this);
    this.emailInput = this.emailInput.bind(this);

  }
  state = { email: '', password: '' }

  passwordInput(password) {
    this.setState({ password })
  }

  emailInput(email) {
    this.setState({ email })
  }

  render() {
    return (
      <Wrapper>
        <Helmet>
          <title>Login</title>
        </Helmet>
        <InnerWrapper>
          <ReactSVG path={Logo} svgStyle={{ width: 152 }} />
          <Text lightBlue medium PernodItalic smallHeight tinySpacing defaultWidth>
            Lorem ipsum dolor sit amet, maiores ornare ac fermentum, imperdiet ut vivamus
            a, nam lectus at nunc.
          </Text>
          <Text RobotoLight white topMargin smallHeight defaultWidth>
            Lorem ipsum dolor sit amet, maiores ornare ac fermentum, imperdiet ut vivamus
            a, nam lectus at nunc. Quam euismod sem, semper ut potenti pellentesque
            quisque. In eget sapien sed, sit duis vestibulum ultricies, placerat morbi
            amet vel, nullam in in lorem vel. In molestie elit dui dictum, praesent
            nascetur pulvinar sed.{' '}
          </Text>

          <InputsWrapper>
            {/* <InputField fieldTitle="Username" passedFunction={this.emailInput} />
            <InputField
              fieldTitle="Password"
              passedFunction={this.passwordInput}
              taipi="password"
            /> */}
            <label for="inp" class="inp login-inp">
                <input
                  type="text"
                  id="inp"
                  placeholder="&nbsp;"
                  // onChange={e => this.props.passedFunction(e.target.value)}
                />
                <span class="label">Username</span>
                {/* <span class="border" /> */}
              </label>
              <label for="inp" class="inp login-inp" id="passInp">
                <input
                  
                  type="password"
                  id="inp"
                  placeholder="&nbsp;"
                  // onChange={e => this.props.passedFunction(e.target.value)}
                />
                <span class="label">Password</span>
                {/* <span class="border" /> */}
              </label>
          </InputsWrapper>
          <div style={{ marginTop: 40, marginBottom: 30 }}>
            <Link to="/feed" style={{ textDecoration: 'none' }}>
              <div class="wrap2">
                <a class="button2 rollover2" > // eslint-disable-line no-unused-vars
                  <span class="roll-text2">LOGIN</span>
                  <span class="roll-text2">LOGIN</span>
                </a>
              </div>
            </Link>
          </div>
        </InnerWrapper>
      </Wrapper>
    )
  }
}

export default Email
