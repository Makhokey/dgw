import styled from 'styled-components'
import color from '../../styles/colors'
import ImportedText from '../../styles/segments'
import ImportedButton from '../../components/BlueCard/Button'
import { giant, phone } from '../../styles/sizes'

var h =
  window.innerHeight ||
  document.documentElement.clientHeight ||
  document.body.clientHeight
let height
if (h > 768) {
  height = h
} else {
  height = 768
}

export const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  min-height: ${height}px;
  background-color: ${color.bgBlue};
  display: flex;
  align-items: center;
  flex-direction: column;
  ${phone} {
    min-height: 100vh;
    height: 100%;
  }
`

export const InnerWrapper = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  ${giant} {
    margin: auto;
    max-width: 700px;
  }
  margin: 40px;
  margin-top: auto;
  margin-bottom: auto;
`

export const Text = styled(ImportedText)`
  ${'' /* color: red; */};
`

export const Button = styled(ImportedButton)`
  margin-top: 40px;
`

export const InputsWrapper = styled.div`
  height: 90px;
`
