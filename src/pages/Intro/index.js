import React, { Component } from 'react'
import Wrapper from './Wrapper'
import AgeGate from '../../components/AgeGate'
import { Helmet } from 'react-helmet'

import { MyContext } from '../../index'

class Intro extends Component {
  render() {
    return (
      <Wrapper>
        <Helmet>
          <title>Intro</title>
        </Helmet>
        <AgeGate />
      
      </Wrapper>
    )
  }
}

export default Intro
