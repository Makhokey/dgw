import styled from 'styled-components'
import bgImage from '../../assets/images/bg.jpg'

const Wrapper = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  min-width: 100px;
  min-height: 100vh;
  height: 100%;
  background-image: url(${bgImage});
  display: flex;
  margin: 0;
  padding: 0;
  justify-content: center;
`

export default Wrapper
