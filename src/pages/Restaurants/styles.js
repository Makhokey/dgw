import styled from 'styled-components'
import Text from '../../styles/segments'
import { giant, tablet, phone } from '../../styles/sizes'

export const Wrapper = styled.div`
  padding-top: 60px;
  flex: 1;
  ${giant} {
    display: flex;
    flex-wrap: wrap;
    padding-left: 360px;
    justify-content: flex-start;
  }
  ${'' /* @media (min-width: 1024px) {
    padding-left: 402px;
  }
  @media (min-width: 1440px) {
    padding-left: 465px;
  } */};
`

export const RestaurantsDistrictLine = styled.div`
  border-bottom: 1px solid black;
  width: 100%;
  margin-bottom: 10px;
  padding-bottom: 5px;
  font-size: 25px;
`

export const RestaurantsWrapper = styled.div`
 display: grid;
 grid-template-columns: minmax(30%, 50%) minmax(30%, 50%);
 box-sizing: border-box;
 grid-gap: 20px;
 padding: 20px;
 ${giant} {
    padding-top: 40px;
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
 }
 ${phone}{
  grid-template-columns: 100%;
 }
 ${tablet}{
  grid-template-columns: 100%;
 }
`

export const FilterButton = styled(Text)`
  width: 140px;
  height: 37px;
  line-height: 37px;
  font-family: RobotoLight;
  margin: auto;
  position: fixed;
  left: 50%;
  transform: translate(-50%, 0);
  bottom: 15px;
  text-align: center;
  display: none;
  background-color: rgb(127, 165, 208);
`

export const MapPageWrapper = styled.div`
  ${'' /* position: absolute;
  width: 100%;
  height: 100%;
  right: 100%;
  top: 0;
  left: 0;
  z-index: 4; */};
`
