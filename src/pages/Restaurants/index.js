import React, { Component } from 'react'
import Header from '../../components/Header'
import RestaurantRowEntry from '../../components/RestaurantRowEntry'
import { Wrapper, FilterButton, RestaurantsWrapper, RestaurantsDistrictLine } from './styles'
import { Helmet } from 'react-helmet'
import Sidebar from '../../components/Sidebar'
import Filter from '../../components/Filter'
import TweenMax from 'gsap'
import axios from 'axios'
import { SidebarContext } from '../../providers/Sidebar'
import Progress from '../../components/Progress'
import { Expo } from 'gsap/EasePack'
import { Link } from 'react-router-dom'
var injectTapEventPlugin = require('react-tap-event-plugin')
injectTapEventPlugin()

class Restaurants extends Component {
  constructor() {
    super()

    this.openFilter = this.openFilter.bind(this)
    this.onChange = this.onChange.bind(this)
    this.filteredProperties = this.filteredProperties.bind(this)
    this.renderRestaurants = this.renderRestaurants.bind(this)
    this.loadItems = this.loadItems.bind(this)
    // this.skroll = this.skroll.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
  }

  state = {
    filterOpened: false,
    properties: [],
    nextPage: '',
    loading: true,
    value: '',
    showFilter: false
  }

  componentDidMount() {
    if (this.props.context2.state.filterOpened) {
      TweenMax.to('#filter', 0, { right: '0%' })
    } else if (!this.props.context2.state.filterOpened) {
      TweenMax.to('#filter', 0, { right: '100%' })
    }
    TweenMax.to('.Select-placeholder', 0, { backgroundColor: 'rgb(0, 10, 50)' })
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  checkAndShowFilter(length) {
    TweenMax.to('#filterButton', 0, { display: 'block' })
    // if (length < 3) {
    //   this.setState({ showFilter: true })
    //   TweenMax.to('#filterButton', 0.4, { bottom: '15px' })
    // } else {
    //   TweenMax.to('#filterButton', 0.4, { bottom: '-40px' })
    // }
  }

  openFilter() {
    TweenMax.to('#filter', 0.7, { right: '0', ease: Expo.easeOut })
    this.setState({ filterOpened: true })
  }

  onChange(title) {
    if (this._timeout) {
      clearTimeout(this._timeout)
    }
    this._timeout = setTimeout(() => {
      if (title) {
        title = title.replace(/\s+/g, '-').toLowerCase()
        axios
          .get(`https://drinkguideadmin.azurewebsites.net/property/title=${title}.json`)
          .then(res => {
            this.checkAndShowFilter(res.data.data.length)
            this.setState({
              properties: res.data.data,
              nextPage: res.data.meta.pagination.links.next
            })
          })

        window.scrollTo({
          top: '0',
          behavior: 'smooth'
        })
      } else {
        axios
          .get('https://drinkguideadmin.azurewebsites.net/properties.json?page=1')
          .then(res => {
            this.checkAndShowFilter(res.data.data.length)
            this.setState({
              loading: false,
              properties: res.data.data,
              nextPage: res.data.meta.pagination.links.next
            })
          })
      }
      this._timeout = null
      this.setState({
        value: title
      })
    }, 500)
  }

  filteredProperties(categories, city, district) {
    TweenMax.to('#filter', 0.4, { right: '100%' })
    if (categories && categories !== undefined) {
      this.setState({ loading: true })
      axios
        .get(
          `https://drinkguideadmin.azurewebsites.net/entries-by-category/?categoryIds=${categories}&cityIds=${city}&districtIds=${district}`
          // `https://absolut-cms.herokuapp.com/web/entries-by-category/?categoryIds=${categories}.json`
        )
        .then(res => {
          this.checkAndShowFilter(res.data.data.length)
          this.setState({ properties: res.data.data, loading: false })
          TweenMax.to('#restaurantIds', 0.2, { display: 'block' })
        })
    }
  }

  renderRestaurants() {
    const {
      title,
      id,
      url,
      email,
      tags,
      number,
      name,
      location,
      district,
      coordinates,
      categories,
      images
    } = this.state.properties

    this.state.properties.sort((a, b) => {
      const nameA = a.district[0].title.toUpperCase()
      const nameB = b.district[0].title.toUpperCase()
      if (nameA > nameB) {
        return -1
      }
      if (nameA < nameB) {
        return 1
      }
      
      return 0
    })


    return (
      <>
        {this.state.properties.map((prop, index) =>
        <>
          {(index === 0 || prop.district[0].title !== this.state.properties[index - 1].district[0].title) && (
            <RestaurantsDistrictLine> { prop.district[0].title }</RestaurantsDistrictLine>
          )}
          <Link
            to={`property/${prop.id}`}
            style={{ textDecoration: 'none', color: 'black' }}
            key={index}
          >
            <RestaurantRowEntry
              name={prop.name}
              tags={prop.tags}
              id={prop.id}
              categories={prop.categories}
              description="this is description"
              website={prop.url}
              email={prop.email}
              mobile={prop.number}
              coordinates="12.34"
              reviews
              image={prop.images[0] && prop.images[0].url}
              rating
              type
              adress={prop.location}
              slider
            />
          </Link>
          </>
        )}
      </>
    )
  }

  loadItems() {
    if (this.state.nextPage) {
      axios.get(this.state.nextPage).then(res => {
        this.setState({
          loading: false,
          properties: [...this.state.properties, ...res.data.data],
          nextPage: res.data.meta.pagination.links.next
        })
      })
    }
  }

  // skroll() {
  //   window.onscroll = () => {
  //     if (window.scrollY > 80 && !this.state.showFilter) {
  //       this.setState({ showFilter: true })
  //       TweenMax.to('#filterButton', 0.4, { bottom: '15px' })
  //     } else if (
  //       window.scrollY < 50 &&
  //       this.state.showFilter &&
  //       this.state.properties.length > 2
  //     ) {
  //       this.setState({ showFilter: false })
  //       TweenMax.to('#filterButton', 0.4, { bottom: '-40px' })
  //     }
  //   }
  // }

  handleScroll() {
    const windowHeight =
      'innerHeight' in window ? window.innerHeight : document.documentElement.offsetHeight
    const body = document.body
    const html = document.documentElement
    const docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    )
    const windowBottom = windowHeight + window.pageYOffset
    if (windowBottom >= docHeight) {
      {
        if (this.state.nextPage && !this.state.loading) {
          this.setState({ loading: true })
          this.loadItems()
        }
      }
    }
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Feed</title>
        </Helmet>
        <Sidebar />
        <Filter passedFunction={this.filteredProperties} />
        <Header passedFunction={this.onChange} />
        <div id="restaurantIds">
          <Wrapper id="restaurants">
            <RestaurantsWrapper>
              {this.renderRestaurants()}
              {/* {this.skroll()} */}
              {this.state.loading && <Progress />}
            </RestaurantsWrapper>
          </Wrapper>
        </div>
        {window.innerWidth < 769 && (
          <FilterButton
            id="filterButton"
            white
            RobotoRegular
            regular
            onTouchTap={this.openFilter}
            onClick={() => {
              TweenMax.to('#filter', 0.4, { right: '0', ease: Expo.easeIn })
              TweenMax.to('#filterButton', 0, { display: 'none' })
              // this.openFilter()
            }}
          >
            FILTER
          </FilterButton>
        )}
      </div>
    )
  }
}

export default React.forwardRef((props, ref) => (
  <SidebarContext.Consumer>
    {context2 => <Restaurants context2={context2} ref={ref} />}
  </SidebarContext.Consumer>
))
