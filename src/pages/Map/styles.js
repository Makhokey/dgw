import styled from 'styled-components'
import Map from '../../assets/images/map.png'

export const MainWrapper = styled.div`
  width: 100%;
  height: 100vh;

  z-index: 5;
`

export const MapImage = styled.div`
  background-image: url(${Map});
  background-size: cover;
  width: 100%;
  min-height: 100%;
`
