import React from 'react'
import GoogleMaps from '../../components/GoogleMaps'
import { MainWrapper, MapImage } from './styles'
import MapHeader from '../../components/MapHeader'
import { Helmet } from 'react-helmet'

const Map = ({}) => {
  return (
    <MainWrapper>
      <Helmet>
        <title>Map</title>
      </Helmet>
      <MapHeader />
      {/* <GoogleMaps fullHeight /> */}
      <MapImage />
    </MainWrapper>
  )
}

export default Map
