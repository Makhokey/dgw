import styled, { keyframes } from 'styled-components'
import ImportedText from '../../styles/segments'
import { giant } from '../../styles/sizes'
import color from '../../styles/colors'

export const DetailsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${giant} {
    ${'' /* margin-right: 10px; */} width: 475px;
  }

  @media (max-width: 769px) {
    width: auto;
    margin: 0 20px;
  }
`
export const InfoLine = styled.div`
  display: flex;
  align-items: center;
  margin: 12px 0;
`

export const Text = styled(ImportedText)`
  margin-bottom: 5px
  :last-child{
    margin-bottom: 0
  }
`

export const UnderLine = styled.div`
  width: 100%;
  border-bottom: 1px solid ${color.lightGray};
`

export const MapWrapper = styled.div`
  margin-top: 18px;
`

export const ColumnsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  ${giant} {
    width: 950px;
    margin: auto;
    flex-direction: row;
    margin-top: 60px;
  }
`

export const CarouselWrapper = styled.div`
  padding: 70px 20px 0 20px;
  margin: 0 15px 15px 0;
  @media (min-width: 769px) {
    padding: 0;
  }
  @media (max-width: 769px) {
    margin: 0;
  }
`

export const FirstColumn = styled.div`
  ${giant} {
    margin-right: 10px;
    width: 475px;
  }
  display: flex;
  flex-direction: column;
`

export const Arrow = styled.div`
  &:hover {
    animation: ${rotationBuilder(10)} 1s linear infinite;
  }
`

export const BackArrowWrapper = styled.div`
  z-index: 4;
  position: absolute;
  cursor: pointer;
  background: rgba(76, 76, 76, 0.7);
  border-radius: 50%;
  width: 32px;
  height: 32px;
  margin-left: 20px;
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;

  ${giant} {
    position: fixed;
    top: -10px;
    left: -10px;
  }
`

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px;
  ${giant} {
    margin: 0;
  }
`

function rotationBuilder(degree) {
  const rotation = keyframes`
 0%,
      20%,
      50%,
      80%,
      100% {
        -ms-transform: translateX(0);
        transform: translateX(0);
      }
      40% {
        -ms-transform: translateX(-12px);
        transform: translateX(-12px);
      }
      ${
        '' /* 60% {
        -ms-transform: translateX(-15px);
        transform: translateX(-15px);
      } */
      }
  `
  return rotation
}
