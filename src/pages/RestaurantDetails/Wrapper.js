import styled from 'styled-components'
import { giant } from '../../styles/sizes'

const Wrapper = styled.div`
  ${'' /* width: 1200px;
  height: 200px;
  border: 1px solid red; */};
  margin: auto;
`

export const MainWrapper = styled.div`
  ${'' /* display: flex;
  justify-content: center; */};

  a {
    color: rgb(76, 76, 76);
  }
  ${giant} {
    width: 1000px;
    margin: auto;
  }
`

export default Wrapper
