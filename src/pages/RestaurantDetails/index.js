import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Wrapper from './Wrapper'
import RestaurantRowEntry from '../../components/RestaurantRowEntry'
import Restaurants from '../Restaurants'
import {
  DetailsWrapper,
  InfoLine,
  UnderLine,
  Text,
  MapWrapper,
  BackArrowWrapper,
  ColumnsWrapper,
  CarouselWrapper,
  FirstColumn,
  InfoWrapper,
  Arrow
} from './styles'
import { withRouter } from 'react-router-dom'
import LinkIcon from '../../assets/icons/Link.svg'
import Mail from '../../assets/icons/Mail.svg'
import Telephone from '../../assets/icons/Telephone.svg'
import Pin from '../../assets/icons/Pin.svg'
import LeftArrow from '../../assets/icons/arrowLeftWhite.svg'
import ReactSVG from 'react-svg'
import Map from '../../components/GoogleMaps'
import axios from 'axios'
import ImagesSlider from '../../components/ImagesSlider'
import { CoordinatesContext } from '../../providers/GooglePlaces'
import { MainWrapper } from './Wrapper'
import Progress from '../../components/Progress'
import Rating from '../../components/Rating'
import { Helmet } from 'react-helmet'

class RestaurantDetails extends Component {
  state = { propertyDetails: [], isLoading: true }

  goBack = props => {
    this.props.history.goBack()
  }

  componentDidMount() {
    axios
      .get(
        `https://drinkguideadmin.azurewebsites.net/property/${
        this.props.match.params.id
        }.json`
      )
      .then(res => {
        this.setState({ propertyDetails: res.data })
      })
    setTimeout(() => this.setState({ isLoading: false }), 500)
  }

  render() {
    const { isLoading } = this.state

    const {
      id,
      title,
      url,
      email,
      number,
      coordinates,
      images,
      location,
      description,
      categories,
      tags,
      name
    } = this.state.propertyDetails

    if (isLoading) {
      return <Progress />
    }

    return (
      <MainWrapper>
        <Helmet>
          <title>{name}</title>
        </Helmet>
        <Link to="/feed" style={{ width: 100 }}>
          <BackArrowWrapper>
            <Arrow>
              <ReactSVG
                path={LeftArrow}
                svgStyle={{
                  width: 8,
                  verticalAlign: 'middle',
                  cursor: 'pointer',
                  marginRight: 2
                }}
              />
            </Arrow>
          </BackArrowWrapper>
        </Link>
        <Wrapper>
          <ColumnsWrapper>
            <FirstColumn>
              <CarouselWrapper>
                <ImagesSlider width={'100%'} images={images} />
              </CarouselWrapper>
              <InfoWrapper>
                <Text PernodRegular large gray tinySpacing>
                  {name}
                </Text>
                <Text
                  RobotoLight
                  italic
                  normal
                  topMarginSmall
                  style={{ lineHeight: 1.3 }}
                >
                  {categories &&
                    categories.map((category, i) => {
                      return categories[i + 1]
                        ? `${category.title}, `
                        : `${category.title}`
                    })}
                </Text>
                
                <Text RobotoLight gray>
                  {description}
                </Text>
              </InfoWrapper>
            </FirstColumn>
            <DetailsWrapper>
              <InfoLine>
                <ReactSVG path={LinkIcon} svgStyle={{ width: 20, marginRight: 13 }} />
                <Text RobotoMedium>
                  <a href={`http://${url}`}>{url}</a>
                </Text>
              </InfoLine>
              <UnderLine />
              <InfoLine>
                <ReactSVG path={Mail} svgStyle={{ width: 20, marginRight: 13 }} />
                <Text RobotoMedium>
                  <a href={`mailto: ${email}`}>{email}</a>
                </Text>
              </InfoLine>
              <UnderLine />
              <InfoLine>
                <ReactSVG path={Telephone} svgStyle={{ width: 20, marginRight: 13 }} />
                <Text RobotoMedium>
                  <a href={`tel:${number}`}>{number}</a>
                </Text>
              </InfoLine>
              <UnderLine />
              <MapWrapper>
                {coordinates && (
                  <Map
                    longtitude={coordinates[0].longtitude}
                    latitude={coordinates[0].latitude}
                  />
                )}
              </MapWrapper>
              <InfoLine>
                <ReactSVG path={Pin} svgStyle={{ width: 20, marginRight: 13 }} />
                <Text RobotoMedium gray>
                  {location}
                </Text>
              </InfoLine>
            </DetailsWrapper>
          </ColumnsWrapper>
        </Wrapper>
      </MainWrapper>
    )
  }
}

export default withRouter(RestaurantDetails)
