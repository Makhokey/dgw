import React, { Component } from 'react'
import ReactSVG from 'react-svg'
import InfoHeader from '../../components/InfoHeader'
import {
  Wrapper,
  MainWrapper,
  InfoLine,
  SmallUnderLine,
  Img,
  TextBox,
  LocalText,
  UnderLine,
  ArrowWrapper
} from './styles'
import arrowDownBlue from '../../assets/icons/arrowDownBlue.svg'
import Sidebar from '../../components/Sidebar'
import Text from '../../styles/segments'
import { Helmet } from 'react-helmet'
import axios from 'axios'

class Info extends Component {
  state = { data: '' }

  componentDidMount() {
    axios.get(`https://drinkguideadmin.azurewebsites.net/helpful-tips.json`).then(res => {
      this.setState({ data: res.data.data[0] })
    })
  }

  render() {
    let helpfulThirdDescription = this.state.data.helpfulThirdDescription
    let helpfulThirdBodyText = this.state.data.helpfulThirdBodyText
    let helpfulSecondDescription = this.state.data.helpfulSecondDescription
    let helpfulSecondBodyText = this.state.data.helpfulSecondBodyText
    let helpfulFourthDescription = this.state.data.helpfulFourthDescription
    let helpfulFirstDescription = this.state.data.helpfulFirstDescription
    let helpfulFirstBodyText = this.state.data.helpfulFirstBodyText
    let helpfulFifthDescription = this.state.data.helpfulFifthDescription

    return (
      <MainWrapper>
        <Helmet>
          <title>Helpful Tips</title>
        </Helmet>
        <Sidebar />
        <InfoHeader />
        <Wrapper paddingTop>
          <LocalText lage darkBlue PernodRegular bigMargin>
            {this.state.data.helpfulFirstTitle}
          </LocalText>
          <UnderLine />
          <Text lightBlue PernodItalic center>
            <div dangerouslySetInnerHTML={{ __html: helpfulFirstDescription }} />{' '}
          </Text>
          <Text gray RobotoLight topMargin center>
            <div dangerouslySetInnerHTML={{ __html: helpfulFirstBodyText }} />{' '}
          </Text>
          <ArrowWrapper>
            <ReactSVG path={arrowDownBlue} svgStyle={{ width: 20, marginRight: 13 }} />
          </ArrowWrapper>
          <ArrowWrapper />
        </Wrapper>
        <Wrapper>
          <TextBox lightBlue PernodItalic center>
            <LocalText extraLage darkBlue PernodBold bigMargin>
              {this.state.data.helpfulSecondTitle}
            </LocalText>
            <UnderLine />
            <div dangerouslySetInnerHTML={{ __html: helpfulSecondDescription }} />{' '}
            <Text gray RobotoLight topMargin center>
              <div dangerouslySetInnerHTML={{ __html: helpfulSecondBodyText }} />{' '}
            </Text>
          </TextBox>
        </Wrapper>
        <Img />
        <Wrapper>
          <TextBox lightBlue PernodItalic center>
            <LocalText extraLage darkBlue PernodBold bigMargin>
              {this.state.data.HelpfulThirdTitle}
            </LocalText>
            <UnderLine />
            <div dangerouslySetInnerHTML={{ __html: helpfulThirdDescription }} />{' '}
            <Text gray RobotoLight topMargin center>
              <div dangerouslySetInnerHTML={{ __html: helpfulThirdBodyText }} />{' '}
            </Text>
          </TextBox>
          <TextBox lightBlue PernodItalic center>
            <LocalText extraLage darkBlue PernodBold bigMargin>
              {this.state.data.helpfulFourthTitle}
            </LocalText>
            <UnderLine />
            <div dangerouslySetInnerHTML={{ __html: helpfulFourthDescription }} />{' '}
          </TextBox>
        </Wrapper>
        <Img />
        <Wrapper>
          <TextBox lightBlue PernodItalic center>
            <LocalText extraLage darkBlue PernodBold bigMargin>
              {this.state.data.helpfulFifthTitle}
            </LocalText>
            <UnderLine />
            <div dangerouslySetInnerHTML={{ __html: helpfulFifthDescription }} />{' '}
          </TextBox>
        </Wrapper>
      </MainWrapper>
    )
  }
}

export default Info
