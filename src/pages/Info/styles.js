import styled from 'styled-components'
import color from '../../styles/colors'
import ImportedText from '../../styles/segments'
import Image from '../../assets/images/happy-man.jpg'
import Text from '../../styles/segments'

export const Wrapper = styled.div`
  height: 100%;
  padding: 0 20px 0;
`

export const MainWrapper = styled.div`
  max-width: 800px;
  height: 100%;
  margin: auto;
  padding-bottom: 40px;
`

export const InfoLine = styled.div`
  display: flex;
  align-items: center;
  margin: 12px 0;
  color: white;
`
export const SmallUnderLine = styled.div`
  width: 95%;
  border-bottom: 1px solid ${color.lightGray};
`

export const Img = styled.div`
  background-image: url(${Image});
  width: 100%;
  height: 200px;
  margin-top: 40px;
  background-size: cover;
`

export const ArrowWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 25px;
  margin-bottom: -30px;
`

export const TextBox = styled(Text)`
  width: 100%;
  height: 100%;
  border: 4px solid rgb(2, 52, 102);
  padding: 30px;
  padding-top: 0px;
  margin-top: 40px;
  box-sizing: border-box;
`

export const LocalText = styled(ImportedText)`
  margin-top: ${props => (props.bigMargin ? '50px' : '')};
  margin-top: 50px;
  margin-bottom: 10px;
  text-align: center;
`

export const UnderLine = styled.div`
  width: 27px;
  border-radius: 10px;
  border-bottom: 2px solid rgb(2, 52, 102);
  margin: auto;
  margin-bottom: 35px;
  margin-top: 15px;
`
