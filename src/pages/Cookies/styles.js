import styled from 'styled-components'

export const Wrapper = styled.div`
  min-height: 100vh;
  width: 100%;
  height: 100%;
  background-color: #000a32;
  padding: 40px 30px;
  a {
    color: silver;
  }
`
