import styled from 'styled-components'
import color from '../../styles/colors'
import sizes from '../../styles/sizes'
import fonts from '../../styles/fonts'
import ImportedInputField from '../InputField'

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  position: fixed;
  z-index: 2;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  height: ${sizes.header};
  background-color: ${color.bgBlue};
`

export const InnerWrapper = styled.div`
  width: 100%;
  padding: 0 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const SearchInnerWrapper = styled.div`
  height: 100%;
  margin-bottom: 20px;
  visibility: hidden;

  z-index: 13;
  position: relative;
`

export const SearchWrapper = styled.div`
  background-color: ${color.bgBlue};
  width: 100%;
  z-index: 1;
  position: fixed;
  visibility: hidden;
  display: flex;
  height: ${sizes.header};
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
