import React, { Component } from 'react'
import TweenMax from 'gsap'
import { Wrapper, InnerWrapper, SearchWrapper, SearchInnerWrapper } from './styles'
import ReactSVG from 'react-svg'
import Text from '../../styles/segments'
import SearchIcon from '../../assets/icons/searchWhite.svg'
import BackArrow from '../../assets/icons/arrowLeftWhite.svg'
import InputField from '../InputField'
import PlacesAutocomplete from '../PlacesAutocomplete'
import { SidebarContext } from '../../providers/Sidebar'
import { withRouter } from 'react-router-dom'

class Header extends Component {
  state = { searchOpened: false }

  openSearchInput() {
    if (!this.state.searchOpened) {
      this.setState({ searchOpened: true })
      TweenMax.to('#searchTab', 0.4, { height: '120px', paddingTop: '60px' })
    } else {
      this.setState({ searchOpened: false })
      TweenMax.to('#searchTab', 0.4, { height: '60px', paddingTop: '0' })
    }
  }

  goBack = props => {
    this.props.history.goBack()
    TweenMax.to('#filter', 0.4, { right: '0' })
    // this.setState({ filterOpened: true })
    TweenMax.to('#restaurantIds', 0, { display: 'none' })
  }

  render() {
    return (
      <div>
        <Wrapper>
          <InnerWrapper>
            <SidebarContext.Consumer>
              {context => (
                <React.Fragment>
                  <div style={{ cursor: 'pointer' }}>
                    <ReactSVG
                      path={BackArrow}
                      onClick={event => {
                        this.goBack()
                        context.filterOpened()
                      }}
                      svgStyle={{ width: 10 }}
                    />
                  </div>
                </React.Fragment>
              )}
            </SidebarContext.Consumer>
            <Text white PernodRegular>
              Map
            </Text>
            <SearchInnerWrapper>
              <ReactSVG
                path={SearchIcon}
                onClick={() => this.openSearchInput()}
                svgStyle={{ width: 20 }}
              />
            </SearchInnerWrapper>
          </InnerWrapper>
        </Wrapper>
        <SearchWrapper id="searchTab">
          <SearchInnerWrapper>
            <PlacesAutocomplete />
          </SearchInnerWrapper>
        </SearchWrapper>
      </div>
    )
  }
}

export default withRouter(Header)
