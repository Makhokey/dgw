import React, { Component } from 'react'
import TweenMax from 'gsap'
import './styles.css'
import Wrapper from '../BlueCard/Wrapper'
import { Redirect } from 'react-router-dom'
import { Link } from 'react-router-dom'
import ReactSVG from 'react-svg'
import TinyHorizontalLine from '../BlueCard/TinyHorizontalLine'

import Logo from '../../assets/logos/Large.svg'

class AgeGate extends Component {
  state = { showAgeGate: true }

  componentDidMount() {
    var name = 'cookie' + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        this.setState({ showAgeGate: false })
        return c.substring(name.length, c.length)
      }
    }
    return
  }

  saveCookie() {
    TweenMax.to('#avp', 0.4, { opacity: 0, display: 'none' })
    var now = new Date()
    var time = now.getTime()
    time += 3600 * 60000
    now.setTime(time)
    document.cookie = 'cookie=checked;' + '; expires=' + now.toUTCString() + '; path=/'
  }

  render() {
    if (!this.state.showAgeGate) {
      return <Redirect to="/feed" />
    }
    return this.state.showAgeGate ? (
      <div id="avp">
          
        <div class="avp-content">
          <ReactSVG path={Logo} svgStyle={{ width: 200, marginBottom: 20 }} />
          
          <div id="avp-header">On Trade Customer Guide</div>
         <TinyHorizontalLine />
          <div class="avp-text">
         
         <p>Welcome to our on trade customer guide. In this guide you will find restaurants, bars, clubs and hotels that serves from the Pernod Ricard portfolio.</p>


<p>By entering this website you confirm that you are above legal drinking age.</p>
          </div>
           <div class="button-wrapper">
            <Link to="/feed" class="btn btn-default btn-lg" id="avp-btn-yes" onClick={this.saveCookie.bind(this)}>ENTER</Link>
          </div>
        </div>
        
        
      </div>
    ) : (
      ''
    )
  }
}

export default AgeGate
