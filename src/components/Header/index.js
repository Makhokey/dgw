import React, { Component } from 'react'
import TweenMax from 'gsap'
import {
  Wrapper,
  InnerWrapper,
  SearchWrapper,
  SearchInnerWrapper,
  IconWrapper
} from './styles'
import ReactSVG from 'react-svg'
import FilterIcon from '../../assets/icons/filterWhite.svg'
import SearchIcon from '../../assets/icons/searchWhite.svg'
import { Expo } from 'gsap/EasePack'
import Text from '../../styles/segments'
import InputField from '../InputField'
import Burger from '../BurgerComponent'
import './styles.css'
import { LocationContext } from '../../providers/HeaderTitle'

class Header extends Component {
  state = { searchOpened: false }

  openSearchInput() {
    if (!this.state.searchOpened) {
      this.setState({ searchOpened: true })
      TweenMax.to('#searchTab', 0.4, { paddingTop: '120px' })
    } else {
      this.setState({ searchOpened: false })
      TweenMax.to('#searchTab', 0.4, { paddingTop: '0' })
    }
  }

  openSidebar() {
    TweenMax.to('#sidebar', 0.7, { right: '0%', ease: Expo.easeOut })
  }

  render() {
    return (
      <LocationContext.Consumer>
        {context => (
          <div>
            <Wrapper>
              <InnerWrapper>
                <IconWrapper onClick={() => this.openSidebar()}>
                  <Burger class="roll-text8" />
                </IconWrapper>
                <Text white PernodRegular tinySpacing>
                  {
                    !this.props.about &&
                    [context.state && context.state.headerCity,
                    context.state && context.state.headerDistrict && context.state.headerDistrict !== 'All' && '  |  ' + context.state.headerDistrict]
                  }
                </Text>
                <IconWrapper>
                  {this.props.about ? (
                    <ReactSVG
                      path={SearchIcon}
                      onClick={() => this.openSearchInput()}
                      svgStyle={{ visibility: 'hidden' }}
                    />
                  ) : (
                    <ReactSVG
                      path={SearchIcon}
                      onClick={() => this.openSearchInput()}
                      svgStyle={{ width: 20 }}
                    />
                  )}
                </IconWrapper>
              </InnerWrapper>
            </Wrapper>
            {!this.props.about && (
              <SearchWrapper id="searchTab">
                <SearchInnerWrapper>
                  {/* <InputField
                    fieldTitle="Search"
                    passedFunction={this.props.passedFunction.bind(this)}
                  /> */}
                  <label for="inp" class="inp">
                    <input
                      type="text"
                      id="inp"
                      placeholder="&nbsp;"
                      onChange={e => this.props.passedFunction(e.target.value)}
                    />
                    <span class="label">Search</span>
                    <span class="border" />
                  </label>
                </SearchInnerWrapper>
              </SearchWrapper>
            )}
          </div>
        )}
      </LocationContext.Consumer>
    )
  }
}

export default Header
