import styled from 'styled-components'
import color from '../../styles/colors'
import sizes from '../../styles/sizes'
import fonts from '../../styles/fonts'
import ImportedInputField from '../InputField'

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  position: fixed;
  z-index: 3;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  height: ${sizes.header};
  background-color: ${color.bgBlue};
`

export const InnerWrapper = styled.div`
  width: 100%;
  padding: 0 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const SearchInnerWrapper = styled.div`
  height: 100%;
  margin-top: -135px;
`

export const SearchWrapper = styled.div`
  background-color: ${color.bgBlue};
  width: 100%;
  z-index: 1;
  position: fixed;
  display: flex;
  height: ${sizes.header};
  ${'' /* margin-top: -50px; */} flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const InputField = styled(ImportedInputField)`
  .CustomizedInputs-container-1 {
    color: red !important;
    align-items: center !important;
    justify-content: center;
    margin-bottom: 20px;
  }
`
export const IconWrapper = styled.div`
  cursor: pointer;
`
