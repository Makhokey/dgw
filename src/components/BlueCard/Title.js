import styled from 'styled-components'

const Title = styled.div`
  color: white;
  font-family: sans-serif;
`

export default Title
