import React from 'react'
import Wrapper from './Wrapper'
import Title from './Title'
import TinyHorizontalLine from './TinyHorizontalLine'
import TextWrapper from './TextWrapper'
import Button from './Button'
import { Link } from 'react-router-dom'
import ReactSVG from 'react-svg'
import Logo from '../../assets/logos/Medium.svg'
import Text from '../../styles/segments'
import './styles.css'

const BlueCard = ({}) => (
  <Wrapper>
    <ReactSVG path={Logo} svgStyle={{ width: 130 }} />
    <div style={{ textAlign: 'center' }}>
      <Text PernodRegular large>
        Customer drink guide
      </Text>
    </div>
    <TinyHorizontalLine />
    <Text bottomMargin PernodItalic lightBlue medium center>
      Lorem ipsum dolor sit amet, maiores ornare ac fermentum, imperdiet ut vivamus a, nam
      lectus at nunc.
    </Text>
    
  </Wrapper>
)

export default BlueCard
