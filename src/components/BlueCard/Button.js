import styled from 'styled-components'

const Button = styled.div`
  width: 90px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  border: 1px solid white;
`

export default Button
