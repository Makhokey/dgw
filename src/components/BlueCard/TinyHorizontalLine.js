import styled from 'styled-components'

const TinyHorizontalLine = styled.div`
  width: 40px;
  border-top: 1.5pt solid white;
  margin-left: -20px;
  margin-top: 16px; 
  left: 50%;
  position: absolute;
`

export default TinyHorizontalLine
