import styled from 'styled-components'

const TextWrapper = styled.div`
  color: rgb(127, 165, 208);
  letter-spacing: 1.2px;
  text-align: center;
`

export default TextWrapper
