import styled from 'styled-components'

const Wrapper = styled.div`
  width: 75%;
  color: white;
  padding: 20px 40px 50px 40px;
  ${'' /* height: 280px; */} display: flex;
  align-self: flex-end;
  margin-bottom: 30px;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  background-color: rgb(0, 10, 50);
`

export default Wrapper
