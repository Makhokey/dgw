import React, { Component } from 'react'
import animation from '../styles/animations'
import TweenMax from 'gsap'

class Counter extends Component {
  state = { value: 0, isLoggedIn: false }

  increment = () => {
    this.setState(prevState => ({
      value: prevState.value + 1
    }))
  }

  decrement = () => {
    this.setState(prevState => ({
      value: prevState.value - 1
    }))
  }

  logout = () => {
    this.setState({
      isLoggedIn: false
    })
  }

  login = () => {
    this.setState({
      isLoggedIn: true
    })
  }

  toggleEntries = () => {
    TweenMax.to('.scientists-list', 1, { opacity: 0 })
  }

  render() {
    const isLoggedIn = this.state.isLoggedIn

    return (
      <div>
        {this.state.value}
        {isLoggedIn ? (
          <button onClick={this.toggleEntries}>Logout</button>
        ) : (
          <button onClick={this.toggleEntries}>Login</button>
        )}
        <div className="scientists-list" onClick={this.toggleEntries}>
          'Nikola', 'Albert', 'Leonard', 'Stephen', 'Max', 'Nikola', 'Albert', 'Leonard',
          'Stephen', 'Max', 'Nikola', 'Albert', 'Leonard', 'Stephen', 'Max'
        </div>
        <button id="scientists-list" onClick={this.decrement}>
          -
        </button>
      </div>
    )
  }
}

export default Counter
