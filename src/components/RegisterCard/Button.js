import styled from 'styled-components'
import colors from '../../styles/colors'
import fonts from '../../styles/fonts'
import sizes from '../../styles/sizes'

const Button = styled.div`
  width: 110px;
  height: 36px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: ${sizes.largeMargin};
  color: ${colors.gray};
  font-size: ${fonts.sizes.regular};
  border: 1px solid ${colors.lightGray};
`

export default Button
