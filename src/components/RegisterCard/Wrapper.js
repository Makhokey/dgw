import styled from 'styled-components'
import colors from '../../styles/colors'
import sizes from '../../styles/sizes'

const Wrapper = styled.div`
  width: 90%;
  height: 100%;
  max-width: 700px;
  padding: 40px 20px;
  margin: ${sizes.smallMargin} 0;
  border: 3px solid rgb(2, 52, 102);
  background-color: ${colors.bgWhite};
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export default Wrapper
