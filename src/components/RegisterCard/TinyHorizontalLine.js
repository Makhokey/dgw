import styled from 'styled-components'
import colors from '../../styles/colors'

const TinyHorizontalLine = styled.div`
  width: 21px;
  border-top: 0.7px solid ${colors.darkBlue};
  margin: 10px;
`

export default TinyHorizontalLine
