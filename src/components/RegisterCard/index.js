import React, { Component } from 'react'
import ReactSVG from 'react-svg'
import Wrapper from './Wrapper'
import TinyHorizontalLine from './TinyHorizontalLine'
import Text from '../../styles/segments.js'
import Button from './Button'
import DownArrow from '../../assets/icons/arrowDownBlue.svg'
import Selector from './Selector'
import TweenMax from 'gsap'
import { Link } from 'react-router-dom'
import { AgeContext } from '../../providers/AgeGate'
import { Redirect } from 'react-router-dom'
import './styles.css'

class RegisterCard extends Component {
  constructor() {
    super()
    this.scrollWin = this.scrollWin.bind(this);
    this.ageGate = this.ageGate.bind(this);
  }

  scrollWin() {
    window.scrollBy({
      top: 300,
      behavior: 'smooth'
    })
  }

  state = {
    eligible: false,
    countryOptions: [
      { value: 'one', label: 'Denmark' },
      { value: 'twdo', label: 'Georgia' },
      { value: 'dfs', label: 'Sweden' },
      { value: 'two', label: 'Norway' },
      { value: 'twfo', label: 'Finland' }
    ],
    birthdayOptions: [
      { value: 1987, label: '1988' },
      { value: 1988, label: '1989' },
      { value: 1989, label: '1990' },
      { value: 1990, label: '1991' },
      { value: 1992, label: '1992' },
      { value: 1993, label: '1993' },
      { value: 1994, label: '1994' },
      { value: 1995, label: '1995' },
      { value: 1996, label: '1996' },
      { value: 1997, label: '1997' },
      { value: 1998, label: '1998' },
      { value: 1999, label: '1999' },
      { value: 2000, label: '2000' },
      { value: 2001, label: '2001' },
      { value: 2003, label: '2003' },
      { value: 2004, label: '2004' },
      { value: 2005, label: '2005' },
      { value: 2006, label: '2006' },
      { value: 2007, label: '2007' },
      { value: 2008, label: '2008' },
      { value: 2009, label: '2009' },
      { value: 2010, label: '2010' }
    ]
  }

  ageGate(context) {
    var today = new Date()
    if (today.getFullYear() - context > 18 && typeof context !== 'string') {
      this.setState({ eligible: true })
    } else if (typeof context !== 'string') {
      this.setState({ eligible: false })
    }
  }

  render() {

    return (
      <Wrapper>
        <Text darkBlue PernodBold>
          Before we reply, we have on additional request.
        </Text>
        <TinyHorizontalLine />
        <Text italic lightBlue medium PernodItalic tinySpacing>
          One of our benchmark values as a Group is our sense of ethics, and we are
          committed to responsible consumption of our products.
        </Text>
        <Text id="scientists-feed" RobotoLight topMargin smallHeight gray>
          Therefore, we need to verify that you are eligible to access this site’s
          contents, in accordance with legal requirements in your country of residence
          (governing the protection of minors, restrictions on advertising, etc.). Please
          register for this first visit to the site. You will not have to register on
          subsequent visits.
        </Text>
        <Text gray wrap justifyCenter flex RobotoLight topMargin smallHeight>
          Please scroll down to
          <Text darkBlue noMargin RobotoMedium>
            &nbsp; register
          </Text>
          &nbsp; for your first visit
        </Text>
        <ReactSVG
          path={DownArrow}
          svgStyle={{ width: 22 }}
          id="first-arrow"
          onClick={() => this.scrollWin()}
        />
        <Selector
          title="Home country"
          ageChecker={this.ageGate}
          id="home-country"
          pushable="true"
          options={this.state.countryOptions}
        />
        <Selector
          ageChecker={this.ageGate}
          title="Date of birth"
          id="birth-date"
          options={this.state.birthdayOptions}
        />
        <AgeContext.Consumer>
          {context =>
            this.state.eligible ? (
              window.innerWidth < 769 ? (
                <div class="wrap3">
                  <a class="button3 rollover3">
                    <Link
                      style={{ textDecoration: 'none', color: 'rgb(76, 76, 76)' }}
                      to="/feed"
                      class="roll-text3"
                      onClick={() => this.ageGate(context.state.age)}
                    >
                      ENTER
                    </Link>
                    <Link
                      style={{ textDecoration: 'none' }}
                      to="/feed"
                      class="roll-text3"
                      onClick={() => this.ageGate(context.state.age)}
                    >
                      ENTER
                    </Link>
                  </a>
                </div>
              ) : (
                <div class="button5">
                  <Link
                    style={{ textDecoration: 'none' }}
                    to="/feed"
                    class="roll-text3"
                    onClick={() => this.ageGate(context.state.age)}
                  >
                    ENTER
                  </Link>
                </div>
              )
            ) : (
              <div class="wrap4">
                <a class="button4">
                  <div class="roll-text4" onClick={() => this.ageGate(context.state.age)}>
                    ENTER
                  </div>
                </a>
              </div>
            )
          }
        </AgeContext.Consumer>

        {/* </Link> */}
      </Wrapper>
    )
  }
}
export default RegisterCard
