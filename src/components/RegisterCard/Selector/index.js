import React, { Component } from 'react'
import Wrapper from './Wrapper'
import Text from '../../../styles/segments'
import DownArrow from '../../../assets/icons/arrowDownBlue.svg'
import ReactSVG from 'react-svg'
import TweenMax from 'gsap'
import DropDown from '../../DropDown'
import './style.css'

class CountrySelector extends Component {
  state = { dropDownOpened: false }

  handleClick(id) {
    if (!this.state.dropDownOpened) {
      TweenMax.to(`#${id}`, 1, { marginTop: '155px' })
      this.setState({ dropDownOpened: true })
    } else {
      TweenMax.to(`#${id}`, 1, { marginTop: '30px' })
      this.setState({ dropDownOpened: false })
    }
  }

  render() {
    return (
      <Wrapper id={this.props.id}>
        <DropDown
          options={this.props.options}
          placeholder={this.props.title}
          id={`This${this.props.id}`}
          pushable={this.props.pushable}
          ageChecker={this.props.ageChecker}
        />
        {/* <Text RobotoRegular lightBlue normal smallSpacing>
          {this.props.title} */}
        {/* </Text> */}
      </Wrapper>
    )
  }
}

export default CountrySelector
