import styled from 'styled-components'
import colors from '../../../styles/colors'
import fonts from '../../../styles/fonts'

const Text = styled.div`
  color: ${colors.lightBlue};
  font-size: ${fonts.sizes.regular};
`

export default Text
