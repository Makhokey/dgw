import styled from 'styled-components'
import colors from '../../../styles/colors'
import sizes from '../../../styles/sizes'

const Wrapper = styled.div`
  width: 97%;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  text-align: start;
  justify-content: space-between;
  ${'' /* border-bottom: 1px solid ${colors.lightBlue}; */}
  margin-top: ${sizes.largeMargin};
`

export default Wrapper
