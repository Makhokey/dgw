import styled from 'styled-components'
import colors from '../../styles/colors'
import fonts from '../../styles/fonts'

const Title = styled.div`
  color: ${colors.darkBlue};
  font-size: ${fonts.sizes.large};
  font-weight: ${fonts.weights.bold};
`

export default Title
