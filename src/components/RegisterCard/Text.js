import styled from 'styled-components'
import colors from '../../styles/colors'
import fonts from '../../styles/fonts'
import sizes from '../../styles/sizes'

// ==============================================================================
//    TEXT
// ==============================================================================

const textColorChooser = ({ white, gray, lightBlue, lightGray, darkBlue }) => {
  if (white) return `${colors.white};`
  else if (gray) return `${colors.grayF};`
  else if (lightGray) return `${colors.lightGray}`
  else if (lightBlue) return `${colors.lightBlue}`
  else if (darkBlue) return `${colors.darkBlue};`
}

const fontFamilyChooser = ({
  PernodRegular,
  PernodItalic,
  PernodBold,
  PernodItalicBold
}) => {
  if (PernodRegular) return `PernodRegular`
  else if (PernodItalic) return `PernodItalic`
  else if (PernodBold) return `PernodBold`
  else if (PernodItalicBold) return `PernodItalicBold`
}

const fontSizeChooser = ({ tiny, small, normal, medium, large, extraLarge }) => {
  if (tiny) return `${fonts.sizes.tiny}`
  else if (small) return `${fonts.sizes.small}`
  else if (normal) return `${fonts.sizes.normal}`
  else if (medium) return `${fonts.sizes.medium}`
  else if (large) return `${fonts.sizes.large}`
  else if (extraLarge) return `${fonts.sizes.extraLarge};`
}

const fontStyleChooser = ({ italic, bold }) => {
  if (italic) return 'italic'
  else if (bold) return 'bold'
}

const Text = styled.div`
  font-family: ${fontFamilyChooser};
  display: ${props => props.flex && 'flex'};
  flex-direction: ${props => props.column && 'column'};
  justify-content: ${props => props.verticalCenter};
  flex-wrap: ${props => props.wrap && 'wrap'};
  font-style: ${fontStyleChooser};
  margin-top: ${props => (props.noMargin ? '0' : `${sizes.smallMargin}`)};
  color: ${textColorChooser};
  font-size: ${fontSizeChooser};
`

export default Text
