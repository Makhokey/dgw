import styled from 'styled-components'
import color from '../../styles/colors'
import sizes from '../../styles/sizes'
import fonts from '../../styles/fonts'
import ImportedInputField from '../InputField'

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  z-index: 2;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  height: ${sizes.header};
  background-color: white;
`

export const InnerWrapper = styled.div`
  width: 100%;
  padding: 0 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const HeaderTitleWrapper = styled.div`
  margin: auto;
  padding-right: 20px;
`
