import React, { Component } from 'react'
import TweenMax from 'gsap'
import { Wrapper, InnerWrapper, HeaderTitleWrapper } from './styles'
import ReactSVG from 'react-svg'
import FilterIcon from '../../assets/icons/filterBlue.svg'
import Text from '../../styles/segments'

class InfoHeader extends Component {
  openSidebar() {
    TweenMax.to('#sidebar', 0.4, { display: 'block', right: '0%' })
  }

  render() {
    return (
      <div>
        <Wrapper>
          <InnerWrapper>
            <ReactSVG
              path={FilterIcon}
              onClick={() => this.openSidebar()}
              svgStyle={{ width: 20, cursor: 'pointer' }}
            />
            <HeaderTitleWrapper>
              <Text darkBlue PernodRegular extraLarge>
                Info
              </Text>
            </HeaderTitleWrapper>
          </InnerWrapper>
        </Wrapper>
      </div>
    )
  }
}

export default InfoHeader
