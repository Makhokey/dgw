import React from 'react'
import { First, Second, Third, Wrapper } from './styles'

const Burger = ({ title }) => (
  <Wrapper>
    <First id="burger" />
    <Second id="burger" />
    <Third id="burger" />
  </Wrapper>
)

export default Burger
