import styled, { keyframes } from 'styled-components'

export const First = styled.div`
  width: 20px;
  height: 2px;
  background-color: white;
  border-radius: 2px;
  transition: width 0.3s;
  margin-top: 4px;
`

export const Second = styled.div`
  width: 13px;
  height: 2px;
  background-color: white;
  margin-top: 4px;
  transition: width 0.3s;
  border-radius: 2px;
`

export const Third = styled.div`
  width: 16px;
  height: 2px;
  background-color: white;
  margin-top: 4px;
  border-radius: px;
  transition: width 0.3s;
`

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 20px;
  transition: width 2s;

  &:hover {
    ${Third} {
      width: 20px;
    }
    ${Second} {
      width: 20px;
    }
  }
`
