import React from 'react'
import Wrapper from './Wrapper'
import ImagesSlider from '../ImagesSlider'

const RestaurantRowEntry = ({
  name,
  tags,
  description,
  website,
  email,
  mobile,
  coordinates,
  address
}) => (
  <Wrapper>
    <ImagesSlider />
    <div>{`my name is${name}`}</div>
  </Wrapper>
)

export default RestaurantRowEntry
