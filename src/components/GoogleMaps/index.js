import React from 'react'
import GoogleMapsWrapper from './GoogleMapsWrapper.js'
import { Marker } from 'react-google-maps'
import { CoordinatesContext } from '../../providers/GooglePlaces'

class Map extends React.Component {
  render() {
    const { latitude, longtitude } = this.props

    const latitudeCoordinate = parseFloat(latitude)
    const longtitudeCoordinate = parseFloat(longtitude)

    return (
      <CoordinatesContext.Consumer>
        {context => (
          <GoogleMapsWrapper
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBw38tKfzXZgtlMSXyGMXLLXYeo2EJPqg&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div
                style={this.props.fullHeight ? { height: `100%` } : { height: '250px' }}
              />
            }
            mapElement={<div style={{ height: `100%` }} />}
            defaultZoom={latitudeCoordinate ? 14 : 11}
            center={{
              lat: latitudeCoordinate ? latitudeCoordinate : context.state.latitude,
              lng: longtitudeCoordinate ? longtitudeCoordinate : context.state.longtitude
            }}
          >
            <Marker position={{ lat: latitudeCoordinate, lng: longtitudeCoordinate }} />
          </GoogleMapsWrapper>
        )}
      </CoordinatesContext.Consumer>
    )
  }
}

export default Map
