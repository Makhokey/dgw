import React, { Component } from 'react'
import {
  Wrapper,
  Img,
  Text,
  RightWrapper,
  LeftWrapper,
  SectionWrapper,
  MainWrapper
} from './styles'
import surati from '../../assets/images/restaurant-1.jpg'
import ImagesSlider from '../ImagesSlider'
import LeftArrow from '../../assets/icons/arrowLeftBlue.svg'
import ReactSVG from 'react-svg'
import LazyLoad from 'react-lazyload'
import PlaceholderComponent from './placeholder'
import { LazyLoadImage } from 'react-lazy-load-image-component'

const RestaurantRowEntry = ({ id, name, tags, categories, image }) => (
  <MainWrapper>
    {/* <LazyLoad height={200} placeholder={<PlaceholderComponent />} once> */}
    <Img style={{width: '100%'}} src={image} class="Profile-image" alt="Profile image" />
    {/* </LazyLoad> */}
    <Wrapper>
      <SectionWrapper>
        <LeftWrapper>
          <Text PernodRegular large gray tinySpacing>
            {name}
          </Text>
          <Text RobotoLight italic normal topMarginSmall style={{ lineHeight: 1.3 }}>
            {categories &&
              categories.map((category, i) => {
                return categories[i + 1] ? `${category.title}, ` : `${category.title}`
              })}
          </Text>
        </LeftWrapper>
        
      </SectionWrapper>
    </Wrapper>
  </MainWrapper>
)

export default RestaurantRowEntry
