import styled from 'styled-components'
import { giant, tablet } from '../../styles/sizes'
import ImportedText from '../../styles/segments'

export const Wrapper = styled.div`
  padding: 25px 18px;
  ${giant} {
    width: 269px;
    padding: 10px;
  }
`

export const MainWrapper = styled.div`
  @media (min-width: 769px) {
    margin-right: 20px;
    margin-bottom: 10px;
    max-width: 280px;
  }
`

export const LeftWrapper = styled.div`
  width: 60%;
`
export const RightWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  width: 40%;
`

export const SectionWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

export const Img = styled.img`
  display: block;
  width: 100vw;
  height: 180px;
  object-fit: cover;

  &:hover {
    opacity: 0.9;
  }
  ${giant} {
    width: 270px;
  }

   ${tablet} {
    height: 290px;
  }


  
`
export const Text = styled(ImportedText)`
  margin-top: ${props => (props.topMarginSmall ? '8px' : '')};
`
