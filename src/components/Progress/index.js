import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import { MainWrapper } from './styles'

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  }
})

function CircularIndeterminate(props) {
  const { classes } = props
  return (
    <MainWrapper>
      <CircularProgress
        className={classes.progress}
        style={{ color: 'rgb(0, 10, 50)' }}
        thickness={3}
        size={35}
      />
    </MainWrapper>
  )
}

CircularIndeterminate.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CircularIndeterminate)
