import styled from 'styled-components'

export const MainWrapper = styled.div`
  @media (min-width: 769px) {
    margin-left: 243px;
  }
  position: fixed;
  bottom: 40px;
  left: 40%;
`
