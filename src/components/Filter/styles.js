import styled from 'styled-components'
import ImportedText from '../../styles/segments'
import color from '../../styles/colors'
import size from '../../styles/sizes'
import giant from '../../styles/sizes'
import tablet from '../../styles/sizes'
import ReactSVG from 'react-svg'
import Bar from '../../assets/icons/Bar.svg'

export const StyledSpinner = styled.svg`
  animation: rotate 2s linear infinite;

  & .Bar {
    stroke: #5652bf;
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }
`

export const FirstWrapper = styled.div`
  position: fixed;
  max-height: 100%;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }

  @media (min-width: 769px) {
    right: 65% !important;
    width: 340px;
    ::-webkit-scrollbar {
      display: none;
    }
    position: fixed;
    max-height: 100%;
    overflow-y: scroll;
    left: 0;
    top: 0;
  }
  @media (max-width: ${size.media.desktop}) {
    right: 100%;
    width: 100%;
    z-index: 5;
  }
  z-index: 3;
`

export const MainWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  height: 100%;
  z-index: 3;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: rgb(0, 10, 50);
  padding-bottom: 30px;
`

export const HeaderWrapper = styled.div`
  display: flex;
  margin-top: 24px;
  height: 40px;
  min-width: 282px;
  position: absolute;
`

export const Wrapper = styled.div`
  width: 100%;
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  text-align: start;
`

export const LocalText = styled(ImportedText)`
  margin-top: ${props => (props.bigMargin ? '50px' : '')};
  margin-top: 15px;
  margin-right: 234px;
  margin-bottom: 9px;
`

export const MenuMoodLocalText = styled(ImportedText)`
  margin-top: 25px;
  margin-right: 85px;
  margin-bottom: 9px;
`

export const MenuDrinkLocalText = styled(ImportedText)`
  margin-top: 15px;
  margin-right: 77px;
  margin-bottom: 9px;
`

export const HeaderLocalText = styled(ImportedText)`
  margin-top: ${props => (props.bigMargin ? '30px' : '')};
  display: flex;
  align-items: center;
`

export const PinIconWrapper = styled.div`
  margin-top: ${props => (props.bigMargin ? '30px' : '')};
  display: flex;
  align-items: center;
  margin-top: 20px;
  margin-right: 145px;
`

export const MapText = styled.div`
  &:hover {
    opacity: 0.8;
  }
`

export const FirstSectionWrapper = styled.div`
  min-width: 282px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`

export const IconsWrapper = styled.div`
  max-width: 282px;
  display: flex;
  flex-wrap: wrap;
`

export const UnderLine = styled.div`
  width: 27px;
  border-radius: 10px;
  border-bottom: 2px solid white;
  margin-right: 255px;
  margin-bottom: 15px;
`

export const Button = styled(ImportedText)`
  width: 135px;
  height: 40px;
  padding-top: 10px;
  margin: 70px 0;
  cursor: pointer;
  text-align: center;
  vertical-align: middle;
  background-color: ${color.lightBlue};
`
