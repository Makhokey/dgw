import React from "react";
import Select from "react-select";
import DownArrow from "../../../assets/icons/arrowDownBlue.svg";
import "./DefaultStyling.css";
import ReactSVG from "react-svg";
import TweenMax from "gsap";
import { LocationContext } from "../../../providers/SelectedDistrict";
import { LocationContext as HeaderContext } from "../../../providers/HeaderTitle";

class App extends React.Component {
  state = {
    selectedOption: ""
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption });
  };

  openArrow(id) {
    TweenMax.to(`#${id}`, 0.4, { transform: "rotateX(180deg)" });
    if (this.props.pushable) {
      TweenMax.to(`#birth-date`, 0.2, { marginTop: "60px" });
    } else {
      TweenMax.to(`#birth-date`, 0.2, { marginBottom: "180px" });
    }
  }

  closeArrow(id) {
    TweenMax.to(`#${id}`, 0.4, { transform: "rotateX(0deg)" });
    if (this.props.pushable) {
      TweenMax.to(`#birth-date`, 0.2, { marginTop: "40px" });
    } else {
      TweenMax.to(`#birth-date`, 0.2, { marginBottom: "5px" });
    }
  }

  arrowRenderer(id, pushable) {
    return (
      <ReactSVG
        path={DownArrow}
        id={id}
        pushable={pushable}
        svgStyle={{ width: 18 }}
      />
    );
  }

  render() {
    const { selectedOption } = this.state;

    return (
      <LocationContext.Consumer>
        {context => (
          <HeaderContext.Consumer>
            {context2 => {
              if (
                this.props.title !== "Distsrict" &&
                !selectedOption &&
                this.props.options[0] &&
                this.props.options[0].label !== undefined
              ) {
                context.selectedCity(
                  this.props.options[0].label,
                  this.props.options[0].id,
                  true
                );
                context2.headerCity(
                  this.props.options[0].label,
                  this.props.options[0].id
                );
                this.setState({
                  selectedOption: this.props.options[0].label
                });
              }
              return this.props.title === "District" ? (
                <Select
                  onOpen={() => this.openArrow(this.props.id)}
                  onClose={() => this.closeArrow(this.props.id)}
                  arrowRenderer={() =>
                    this.arrowRenderer(this.props.id, this.props.pushable)
                  }
                  placeholder={this.props.placeholder}
                  name="form-field-name"
                  searchable={false}
                  value={selectedOption}
                  onChange={e => {
                    this.setState({ selectedOption: e.label });
                    context.selectedDistrict(e.label, e.id);
                    context2.headerDistrict(e.label, e.id);

                    if (window.innerWidth > 769) {
                      this.props.filterSearch(
                        this.props.selectedCategory,
                        context.state.selectedCityId,
                        e.id
                      );
                    }
                  }}
                  options={this.props.options}
                />
              ) : (
                <Select
                  onOpen={() => this.openArrow(this.props.id)}
                  onClose={() => this.closeArrow(this.props.id)}
                  arrowRenderer={() =>
                    this.arrowRenderer(this.props.id, this.props.pushable)
                  }
                  placeholder={this.props.placeholder}
                  name="form-field-name"
                  searchable={false}
                  value={selectedOption}
                  onChange={async e => {
                    this.setState({ selectedOption: e.label });
                    context.selectedCity(e.label, e.id, false);
                    context2.headerCity(e.label, e.id);

                    if (window.innerWidth > 769) {
                      this.props.filterSearch(
                        this.props.selectedCategory,
                        e.id,
                        context.state.selectedDistrictId
                      );
                    }
                  }}
                  options={this.props.options}
                />
              );
            }}
          </HeaderContext.Consumer>
        )}
      </LocationContext.Consumer>
    );
  }
}

export default App;
