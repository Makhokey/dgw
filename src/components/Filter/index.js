import React, { Component } from "react";
import TweenMax from "gsap";
import {
  LocalText,
  MenuDrinkLocalText,
  MenuMoodLocalText,
  MainWrapper,
  HeaderWrapper,
  Wrapper,
  IconsWrapper,
  FirstSectionWrapper,
  HeaderLocalText,
  UnderLine,
  FirstWrapper,
  MapText
} from "./styles";
import ReactSVG from "react-svg";
import arrowLeftWhite from "../../assets/icons/arrowLeftWhite.svg";
import Text from "../../styles/segments";
import Selector from "./Selector";
import Bar from "../../assets/icons/Bar.svg";
import Champagne from "../../assets/icons/Champagne.svg";
import Club from "../../assets/icons/Club.svg";
import Coctails from "../../assets/icons/Coctails.svg";
import Hotel from "../../assets/icons/Hotel.svg";
import Restaurant from "../../assets/icons/Restaurant.svg";
import Spirit from "../../assets/icons/Spirit.svg";
import Wine from "../../assets/icons/Wine.svg";
import Summer from "../../assets/icons/Summer.svg";
import { SidebarContext } from "../../providers/Sidebar";
import { Expo } from "gsap/EasePack";
import { LocationContext } from "../../providers/SelectedDistrict";
import axios from "axios";

import "./styles.css";

class Filter extends Component {
  constructor() {
    super();
    this.changeIconColor = this.changeIconColor.bind(this);
    this.goToMap = this.goToMap.bind(this);
    this.changeIconColorByContext = this.changeIconColorByContext.bind(this);
    this.selectCategory = this.selectCategory.bind(this);
    this.selectedWhiter = this.selectedWhiter.bind(this);
    this.closeFilter = this.closeFilter.bind(this);
    this.scrollWin = this.scrollWin.bind(this);
    this.searchButtonHandle = this.searchButtonHandle.bind(this);
    this.handleFilterSearch = this.handleFilterSearch.bind(this);
    this.handleInitialLoad = this.handleInitialLoad.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getDistricts = this.getDistricts.bind(this);
    this.filterUpdate = this.filterUpdate.bind(this);
  }

  state = {
    selectedCategory: [],
    cityOptions: [],
    districtOptions: [],
    whiteIcons: [],
    whiteShapes: [],
    selectedCity: '',
  };

  onChange() {

  }

  changeIconColor(icon, shapes) {
    TweenMax.to(`${icon}`, 0, {
      fill: "white"
    });
    TweenMax.to(`${shapes}`, 0, {
      stroke: "white"
    });
  }

  componentDidUpdate() {
    this.state.whiteIcons.map(icon => {
      TweenMax.to(`${icon}`, 0, {
        fill: "white"
      });
    });
    this.state.whiteShapes.map(icon => {
      TweenMax.to(`${icon}`, 0, {
        stroke: "white"
      });
    });
  }

  filterUpdate(a,b,c){

    this.setState({
        selectedCity: b,
    },
    () => {
      this.props.passedFunction(a,b,c);
    })
  }

  getDistricts(){
    if (this.state.selectedCity === '') return this.state.districtOptions;
    return this.state.districtOptions.filter(o => parseInt(o.parent) == parseInt(this.state.selectedCity));
  }

  goToMap() {
    return (
      <SidebarContext.Consumer>
        {context => (
          <MapText>
            <Text
              small
              white
              RobotoRegular
              bigMargin
              onClick={context.filterOpened}
            >
              View Map of Districts
            </Text>
          </MapText>
        )}
      </SidebarContext.Consumer>
    );
  }

  changeIconColorByContext(context, fillIcons, strokeIcons) {
    context.addFillIcons(fillIcons, strokeIcons);
  }

  componentDidMount() {
    this.callCityService();
    this.callDistrictService();
  }

  callCityService = () => {
    axios
      .get("https://drinkguideadmin.azurewebsites.net/city-district.json")
      .then(res => {
        res.data.data.map(city => {
          city.label = city.title;
          city.value = city.title;
        });
        const cityData = res.data.data
        const cityDataFilter = cityData.filter(n => n.parent == null)
        console.log("City Log:", cityDataFilter)
        this.setState({
          cityOptions: cityDataFilter,
          isSelected: true
        });
      })
};

callDistrictService = () => {
  axios
    .get("https://drinkguideadmin.azurewebsites.net/city-district.json")
    .then(res => {
      res.data.data.map(district => {
        district.label = district.title;
        district.value = district.title;
      });
      res.data.data.unshift({ value: "All", label: "All", id: "" });
      const districtData = res.data.data;
      const districtDataFilter = districtData.filter(m => m.parent)
      console.log("District Log", districtDataFilter);
      this.setState({
        districtOptions: districtDataFilter
      });
    })
};

  selectCategory(categoryId) {
    if (this.state.selectedCategory.indexOf(categoryId) < 0) {
      this.state.selectedCategory.push(categoryId);

    } else {
      this.state.selectedCategory.splice(
        this.state.selectedCategory.indexOf(categoryId),
        1
      );
    }
  }

  selectedWhiter(context) {
    context.state.fillIcons.map(idSelector => {
      TweenMax.to(`#Bar`, 0, {
        fill: "white"
      });
    });
  }

  closeFilter() {
    TweenMax.to("#restaurantIds", 0.2, { display: "block" });
    TweenMax.to("#filter", 0.5, { right: "100%", ease: Expo.easeIn });
    TweenMax.to("#filterButton", 0, { display: "block" });
  }

  scrollWin() {
    window.scrollBy({
      top: 0,
      behavior: "smooth"
    });
  }

  searchButtonHandle(context) {
    this.setState({
      whiteIcons: context.state.fillIcons,
      whiteShapes: context.state.strokeIcons
    });
  }

  handleInitialLoad(context, context2) {
    if (context2.state.firstChange) {
      this.props.passedFunction(
        this.state.selectedCategory,
        context2.state.selectedCityId,
        context2.state.selectedDistrictId
      );
      context2.state.firstChange = false;
      this.scrollWin();
      this.searchButtonHandle(context);
    }
  }

  handleFilterSearch(context, context2) {
    this.props.passedFunction(
      this.state.selectedCategory,
      context2.state.selectedCityId,
      context2.state.selectedDistrictId
    );
    this.scrollWin();
    this.searchButtonHandle(context);
  }


  render() {
    console.log(this.props.passedFunction);
    return (
      <SidebarContext.Consumer>
        {context => (
          <LocationContext.Consumer>
            {context2 => (
              <FirstWrapper id="filter">
                {this.handleInitialLoad(context, context2)}
                <MainWrapper>
                  {this.selectedWhiter(context)}
                  <HeaderWrapper>
                    <ReactSVG
                      path={arrowLeftWhite}
                      svgStyle={{ width: 10 }}
                      onClick={() => this.closeFilter()}
                    />
                  </HeaderWrapper>
                  <Wrapper>
                    <HeaderLocalText extraLarge white PernodRegular bigMargin>
                      Filter
                    </HeaderLocalText>
                    <FirstSectionWrapper>
                      <LocalText lage white PernodRegular bigMargin>
                        Where
                      </LocalText>
                      <UnderLine />
                      <Selector
                        title="City"
                        id="city"
                        pushable="true"
                        options={this.state.cityOptions}
                        selectedCategory={this.state.selectedCategory}
                        filterSearch={this.filterUpdate}
                      />
                      <Selector
                        title="District"
                        id="district"
                        options={this.getDistricts()}
                        selectedCategory={this.state.selectedCategory}
                        filterSearch={this.props.passedFunction}
                      />
                    </FirstSectionWrapper>


                    <MenuMoodLocalText large white PernodRegular>
                      Type of establishment
                    </MenuMoodLocalText>
                    <UnderLine />
                    <IconsWrapper>
                      <div class="wrap8">
                        <a class="iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Bar}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              fill: "red",
                              marginRight: 8,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Bar",
                                "#Group-13, #Rectangle-3-Copy-2"
                              );
                              this.selectCategory(2);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8" id="restIcon">
                        <a class="iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Restaurant}
                            param1="#Hotel"
                            param2="#Hotel"
                            onClick={() => {
                              this.selectCategory(3);
                              this.changeIconColorByContext(
                                context,
                                "#Restaurant",
                                "#Group-15, #Rectangle-3-Copy"
                              );
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                            svgStyle={{
                              width: 106,
                              height: 90,
                              stroke: "red",
                              marginRight: 8,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8 hotelIcon"
                            path={Hotel}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Hotel",
                                "#Group-3, #Rectangle-3-Copy-3"
                              );
                              this.selectCategory(4);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                          />

                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Club}
                            onClick={() => {
                              this.selectCategory(5);
                              this.changeIconColorByContext(
                                context,
                                "#Club",
                                "#Group-22, #Rectangle-3-Copy-5"
                              );
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              marginRight: 8,
                              cursor: "pointer"
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Summer}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Summer",
                                "#Group-5, #Rectangle-3-Copy-4"
                              );
                              this.selectCategory(54);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                            svgStyle={{
                              width: 106,
                              height: 90,
                              marginRight: 5,
                              cursor: "pointer"
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8 hotelIcon"
                            path={Coctails}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              cursor: "pointer"
                            }}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Cocktails",
                                "#Group-19, #Rectangle-3-Copy-8"
                              );
                              this.selectCategory(59);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                          />
                        </a>
                      </div>

                    </IconsWrapper>
                    <MenuDrinkLocalText bigMargin large white PernodRegular>
                      Pernod Ricard Portfolio
                    </MenuDrinkLocalText>
                    <UnderLine />
                    <IconsWrapper>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Wine}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              marginRight: 8,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Wine",
                                "#Group-4, #Rectangle-3-Copy-9"
                              );
                              this.selectCategory(55);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8"
                            path={Champagne}
                            svgStyle={{
                              width: 106,
                              height: 90,
                              marginRight: 8,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Champagne",
                                "#Group-9, #Rectangle-3-Copy-6"
                              );
                              this.selectCategory(56);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                          />
                        </a>
                      </div>
                      <div class="wrap8">
                        <a class="iconca iconca">
                          <ReactSVG
                            class="roll-text8 hotelIcon"
                            path={Spirit}
                            svgStyle={{
                              width: 80,
                              height: 90,
                              marginBottom: 4,
                              cursor: "pointer"
                            }}
                            onClick={() => {
                              this.changeIconColorByContext(
                                context,
                                "#Spirit",
                                "#Group-20, #Rectangle-3-Copy-11"
                              );
                              this.selectCategory(57);
                              if(window.innerWidth > 769) this.handleFilterSearch(context, context2);
                            }}
                          />
                        </a>
                      </div>

                    </IconsWrapper>
                  </Wrapper>
                  {
                    window.innerWidth < 769 ? (
                      <div
                        class="wrap10"
                        onClick={e => {
                          this.handleFilterSearch(context, context2);
                        }}
                      >
                        <a class="button10 rollover10">
                          <span class="roll-text10">Filter</span>
                          <span class="roll-text10">Filter</span>
                        </a>
                      </div>
                    ) : null
                  }

                </MainWrapper>
              </FirstWrapper>
            )}
          </LocationContext.Consumer>
        )}
      </SidebarContext.Consumer>
    );
  }
}


export default Filter;
