import React from 'react'
import Select from 'react-select'
import DownArrow from '../../assets/icons/arrowDownBlue.svg'
import './DefaultStyling.css'
import ReactSVG from 'react-svg'
import TweenMax from 'gsap'
import { AgeContext } from '../../providers/AgeGate'

class App extends React.Component {
  state = {
    selectedOption: ''
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
  }

  openArrow(id) {
    TweenMax.to(`.Select-value`, 0, { backgroundColor: 'white' })

    TweenMax.to(`#${id}`, 0.4, { transform: 'rotateX(180deg)' })
    if (this.props.pushable) {
      TweenMax.to(`#birth-date`, 0.2, { marginTop: '200px' })
    } else {
      TweenMax.to(`#birth-date`, 0.2, { marginBottom: '180px' })
    }
  }

  closeArrow(id) {
    TweenMax.to(`#${id}`, 0.4, { transform: 'rotateX(0deg)' })
    if (this.props.pushable) {
      TweenMax.to(`#birth-date`, 0.2, { marginTop: '40px' })
    } else {
      TweenMax.to(`#birth-date`, 0.2, { marginBottom: '40px' })
    }
  }

  arrowRenderer(id, pushable) {
    return (
      <ReactSVG path={DownArrow} id={id} pushable={pushable} svgStyle={{ width: 18 }} />
    )
  }

  render() {
    const { selectedOption } = this.state
    
    return (
      <AgeContext.Consumer>
        {context => (
          <Select
            onOpen={() => this.openArrow(this.props.id)}
            onClose={() => this.closeArrow(this.props.id)}
            arrowRenderer={() => this.arrowRenderer(this.props.id, this.props.pushable)}
            placeholder={this.props.placeholder}
            name="form-field-name"
            searchable={false}
            value={selectedOption}
            onChange={e => {
              TweenMax.to(`.Select-value`, 0, { backgroundColor: 'white' })
              this.props.ageChecker(e.value)
              this.setState({ selectedOption: e.value })
              context.setAge(e.value)
            }}
            options={this.props.options}
          />
      )}
      </AgeContext.Consumer>
    )
  }
}

export default App
