import React from 'react'
import { MainWrapper, ArrowWrapper, UnderLine } from './styles'
import ReactSVG from 'react-svg'
import { Link } from 'react-router-dom'
import Logo from '../../assets/logos/Color.svg'
import arrowLeftBlue from '../../assets/icons/arrowLeftBlue.svg'
import { Text, Wrapper } from './styles'
import TweenMax from 'gsap'
import { Expo } from 'gsap/EasePack'
import './styles.css'

const Sidebar = ({}) => {
  function closeSidebar() {
    TweenMax.to('#sidebar', 0.5, { right: '100%', ease: Expo.easeIn })
  }
  return (
    <MainWrapper id="sidebar">
      <ArrowWrapper style={{ width: 100, marginLeft: -15, marginTop: -8 }}>
        <ReactSVG
          path={arrowLeftBlue}
          svgStyle={{ width: 10, marginLeft: '14px' }}
          onClick={() => closeSidebar()}
        />
      </ArrowWrapper>
      {window.innerWidth < 769 ? (
        <Wrapper>
          <ReactSVG path={Logo} svgStyle={{ width: 200 }} />
          <div style={{ height: 20 }} />

          <Link
            to="/feed"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            <Text extraLarge darkBlue PernodBold bigMargin>
              On Trade guide
            </Text>
          </Link>
          
          <Link
            to="/about"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            {' '}
            <Text extraLarge darkBlue PernodBold bigMargin>
              Contact
            </Text>
          </Link>
          <div style={{ height: 20 }} />

          <Link
            to="/privacy-policy"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            <Text darkBlue PernodItalic large bigMargin>
              Privacy Policy
            </Text>
          </Link>
          <Link
            to="/responsibility-statement"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            <Text darkBlue PernodItalic large bigMargin>
              Responsibility Statement
            </Text>
          </Link>
          <Link
            to="/terms-and-conditions"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            <Text darkBlue PernodItalic large bigMargin>
              Terms & Conditions
            </Text>
          </Link>
          <Link
            to="/cookies"
            style={{ textDecoration: 'none' }}
            onClick={() => closeSidebar()}
          >
            <Text darkBlue PernodItalic large bigMargin>
              Cookies
            </Text>
          </Link>
        </Wrapper>
      ) : (
        <Wrapper>
          <ReactSVG path={Logo} svgStyle={{ width: 200 }} />
          <div style={{ height: 20 }} />
          <div className="wrap6" onClick={() => closeSidebar()}>
            <div className="button6 rollover6">
              <Link to="/feed" style={{ textDecoration: 'none' }} className="roll-text6">
                <Text extraLarge darkBlue PernodBold>
                On Trade guide
                </Text>
              </Link>
              <Link
                to="/feed"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text extraLarge darkBlue PernodBold>
                  On Trade guide
                </Text>
                <UnderLine dark />
              </Link>
            </div>
          </div>
          
          <div className="wrap6" onClick={() => closeSidebar()}>
            <div className="button6 rollover6">
              <Link to="/about" style={{ textDecoration: 'none' }} className="roll-text6">
                <Text extraLarge darkBlue PernodBold>
                  Contact
                </Text>
              </Link>
              <Link
                to="/about"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text extraLarge darkBlue PernodBold>
                  Contact
                </Text>
                <UnderLine dark />
              </Link>
            </div>
          </div>
          <div style={{ height: 30 }} />

          <div className="wrap6">
            <div className="button6 rollover6">
              <Link
                to="/privacy-policy"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
              >
                <Text darkBlue PernodItalic large>
                  Privacy Policy
                </Text>
              </Link>
              <Link
                to="/privacy-policy"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text darkBlue PernodItalic large>
                  Privacy Policy
                </Text>
                <UnderLine />
              </Link>
            </div>
          </div>
          <div className="wrap6">
            <div className="button6 rollover6">
              <Link
                to="/responsibility-statement"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
              >
                <Text darkBlue PernodItalic large>
                  Responsibility Statement
                </Text>
              </Link>
              <Link
                to="/responsibility-statement"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text darkBlue PernodItalic large>
                  Responsibility Statement
                </Text>
                <UnderLine />
              </Link>
            </div>
          </div>
          <div className="wrap6">
            <div className="button6 rollover6">
              <Link
                to="/terms-and-conditions"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
              >
                <Text darkBlue PernodItalic large>
                  Terms & Conditions
                </Text>
              </Link>
              <Link
                to="/terms-and-conditions"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text darkBlue PernodItalic large>
                  Terms & Conditions
                </Text>
                <UnderLine />
              </Link>
            </div>
          </div>
          <div className="wrap6">
            <div className="button6 rollover6">
              <Link
                to="/cookies"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
              >
                <Text darkBlue PernodItalic large>
                  Cookies
                </Text>
              </Link>
              <Link
                to="/cookies"
                style={{ textDecoration: 'none' }}
                className="roll-text6"
                onClick={() => closeSidebar()}
              >
                <Text darkBlue PernodItalic large>
                  Cookies
                </Text>
                <UnderLine />
              </Link>
            </div>
          </div>
        </Wrapper>
      )}
    </MainWrapper>
  )
}

export default Sidebar
