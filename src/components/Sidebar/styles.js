import styled, { keyframes } from 'styled-components'
import color from '../../styles/colors'
import size from '../../styles/sizes'
import ImportedText from '../../styles/segments'
import BackgroundImage from '../../assets/images/bottles-2.jpg'

export const MainWrapper = styled.div`
  width: 100%;
  height: 100vh;
  right: 100%;
  height: 100%;
  
  background-color: white;
  background-image: url(${BackgroundImage});
  background-size: cover;
  background-position: top center;
  box-sizing: border-box;
  border: 7px solid ${color.darkBlue};
  position: fixed;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
  z-index: 4;
`

export const ArrowWrapper = styled.div`
  padding-top: 20px;
  padding-left: 20px;
  cursor: pointer;
  display: inline-block;
  width: 60px;
  &:hover {
    animation: ${rotationBuilder(10)} 1s linear infinite;
  }
`

function rotationBuilder(degree) {
  const rotation = keyframes`
 0%,
      20%,
      50%,
      80%,
      100% {
        -ms-transform: translateX(0);
        transform: translateX(0);
      }
      40% {
        -ms-transform: translateX(-15px);
        transform: translateX(-15px);
      }
      ${
        '' /* 60% {
        -ms-transform: translateX(-15px);
        transform: translateX(-15px);
      } */
      }
  `
  return rotation
}

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Text = styled(ImportedText)`
  margin-top: ${props => (props.bigMargin ? '30px' : '0')};
`

export const UnderLine = styled.div`
  width: 100%;
  border-radius: 10px;
  border-bottom: 2px solid
    ${props => (props.dark ? 'rgb(2, 52, 102)' : 'rgb(127,165,208)')};
  margin: auto;
  ${'' /* margin-bottom: 35px;
  margin-top: 15px; */};
`
