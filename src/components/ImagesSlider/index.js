import React from 'react'
import Carousel from 'nuka-carousel'
import bottles from '../../assets/images/bottles.jpg'
import happyMan from '../../assets/images/restaurant-3.jpg'
import Rest1 from '../../assets/images/happy-man.jpg'
import Rest2 from '../../assets/images/restaurant-1.jpg'
import Rest3 from '../../assets/images/restaurant-2.jpg'

import './styles.css'

export default class extends React.Component {
  render() {
    return (
      <Carousel width={this.props.width}>
        {this.props.images &&
          this.props.images.map((image, index) => {
            return <img src={image.url} key={index} />
          })}
        {/* <img src={this.props.images[0]} /> */}
      </Carousel>
    )
  }
}
