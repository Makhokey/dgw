import React from 'react'
import { CoordinatesContext } from '../../providers/GooglePlaces'
import './styles.css'

const { compose, withProps, lifecycle } = require('recompose')
const { withScriptjs } = require('react-google-maps')

const {
  StandaloneSearchBox
} = require('react-google-maps/lib/components/places/StandaloneSearchBox')

const PlacesWithStandaloneSearchBox = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyDPZVTVaoFFBM-NJZUiOqkr8-qL21yf3nc&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />
  }),
  lifecycle({
    componentWillMount() {
      const refs = {}

      this.setState({
        places: [],
        onSearchBoxMounted: ref => {
          refs.searchBox = ref
        },
        onPlacesChanged: (props, context) => {
          const places = refs.searchBox.getPlaces()
          this.setState({
            places
          })
          places.map(({ geometry: { location } }) =>
            context.setCoordinates(location.lat(), location.lng())
          )
        }
      })
    }
  }),
  withScriptjs
)(props => (
  <CoordinatesContext.Consumer>
    {context => (
      <div data-standalone-searchbox="">
        <StandaloneSearchBox
          ref={props.onSearchBoxMounted}
          bounds={props.bounds}
          onPlacesChanged={() => props.onPlacesChanged(props, context)}
        >
          <input
            type="text"
            placeholder="Search Location"
            style={{
              boxSizing: `border-box`,
              border: 'none',
              borderBottom: '0.3px solid #7fa5d0',
              width: `240px`,
              height: `32px`,
              padding: `0 12px`,
              backgroundColor: '#000a32',
              color: '#7fa5d0',
              fontSize: `14px`,
              outline: `none`,
              textOverflow: `ellipses`
            }}
          />
        </StandaloneSearchBox>
      </div>
    )}
  </CoordinatesContext.Consumer>
))

export default PlacesWithStandaloneSearchBox
