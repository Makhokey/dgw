import React, { Component } from 'react'
import './styles.css'
import {
  RatingCircle,
  OuterRatingCircle,
  OuterRatingCircle2
} from './styles.js'
import circle from '../../assets/icons/rating.png'

class App extends Component {
  render() {
    return <Rating />
  }
}

class Rating extends Component {
  state = {
    rating: this.props.rating || 1
  }
  rate(rating) {
    this.setState({
      rating: rating
    })
  }

  render() {
    var stars = []

    for (var i = 0; i < 5; i++) {
      var klass = 'star-rating__star'

      if (this.state.rating >= i && this.state.rating != null) {
        klass += ' is-selected'
      }

      stars.push(
        <label
          className={klass}
          key={i}
        >
          <RatingCircle>
            <OuterRatingCircle>
              <OuterRatingCircle2 />
            </OuterRatingCircle>
          </RatingCircle>
        </label>
      )
    }

    return <div className="star-rating">{stars}</div>
  }
}

export default App
