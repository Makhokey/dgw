import styled from 'styled-components'

export const RatingCircle = styled.div`
  width: 15px;
  height: 15px;
  border: 2px solid rgb(127, 165, 208);
  position: relative;
  border-radius: 50%;
  color: blue;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const OuterRatingCircle = styled.div`
  width: 9px;
  height: 9px;
  background-color: white;
  border-radius: 50%;
  color: blue;
`

export const OuterRatingCircle2 = styled.div`
  width: 9px;
  height: 9px;
  background-color: rgb(127, 165, 208);
  border-radius: 50%;
  color: blue;
`

export const outerCircle = styled.div`
  background-color: rgb(127, 165, 208);
  border-radius: 50%;
  height: 50px;
  width: 50px;
  position: relative;
  /* 
     Child elements with absolute positioning will be 
     positioned relative to this div 
    */
`

export const innerCircle = styled.div`
  position: absolute;
  background: red;
  border-radius: 50%;
  height: 20px;
  width: 20px;
  /*
     Put top edge and left edge in the center
    */
  top: 50%;
  left: 50%;
  margin: -150px 0px 0px -150px;
  /* 
     Offset the position correctly with
     minus half of the width and minus half of the height 
    */
`
