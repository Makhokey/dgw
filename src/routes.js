import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import App from './pages/Home/index'
import RestaurantDetails from './pages/RestaurantDetails'
import Restaurants from './pages/Restaurants'
import Intro from './pages/Intro'
import Register from './pages/Register'
import test from './components/test'
import signup from './pages/SignUp'
import About from './pages/About'
import Input from './components/InputField'
import Map from './pages/Map'
import Info from './pages/Info'
import Cookies from './pages/Cookies'
import ResponsibilityStatement from './pages/ResponsibilityStatement'
import Tos from './pages/TOS'
import PrivacyPolicy from './pages/PrivacyPolicy'
import mappia from './components/PlacesAutocomplete'
import Info2 from './components/Context/app.js'

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Intro} />
      <Route path="/property/:id" component={RestaurantDetails} />
      <Route path="/feed" component={Restaurants} />
      <Route path="/intro" component={Intro} />
      <Route path="/register" component={Register} />
      <Route path="/test" component={test} />
      <Route path="/input" component={Input} />
      <Route path="/login" component={signup} />
      <Route path="/about" component={About} />
      <Route path="/map" component={Map} />
      <Route path="/helpful-tips" component={Info} />
      <Route path="/cookies" component={Cookies} />
      <Route path="/responsibility-statement" component={ResponsibilityStatement} />
      <Route path="/terms-and-conditions" component={Tos} />
      <Route path="/privacy-policy" component={PrivacyPolicy} />
      <Route path="/info2" component={Info2} />
    </Switch>
  </BrowserRouter>
)

export default Routes
