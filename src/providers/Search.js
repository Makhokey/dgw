import React, { Component } from 'react'

export const SearchContext = React.createContext()

class SearchProvider extends Component {
  state = {
    selectedCategories: ''
  }

  render() {
    return (
      <SearchContext.Provider
        value={{
          state: this.state,
          selectedCategories: categories =>
            this.setState({ selectedCategories: categories })
        }}
      >
        {this.props.children}
      </SearchContext.Provider>
    )
  }
}

export default SearchProvider
