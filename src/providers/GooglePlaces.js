import React, { Component } from 'react'

export const CoordinatesContext = React.createContext()

class CoordinatesProvider extends Component {
  state = {
    latitude: 59.326242,
    longtitude: 17.8419706
  }

  render() {
    return (
      <CoordinatesContext.Provider
        value={{
          state: this.state,
          setCoordinates: (lat, lng) => {
            this.setState({ latitude: lat, longtitude: lng })
          }
        }}
      >
        {this.props.children}
      </CoordinatesContext.Provider>
    )
  }
}

export default CoordinatesProvider
