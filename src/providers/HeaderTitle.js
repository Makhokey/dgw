import React, { Component } from 'react'

export const LocationContext = React.createContext()

class SelectRegion extends Component {
  state = {
    headerCity: '',
    headerCityId: '',
    headerDistrict: '',
    headerDistrictId: '',
  }

  render() {
    return (
      <LocationContext.Provider
        value={{
          state: this.state,
          headerCity: (city, cityId) => this.setState({ headerCity: city, headerCityId: cityId }),
          headerDistrict: (district, districtId) => this.setState({ headerDistrict: district, headerDistrictId: districtId }),
        }}
      >
        {this.props.children}
      </LocationContext.Provider>
    )
  }
}

export default SelectRegion