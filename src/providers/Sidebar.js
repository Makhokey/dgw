import React, { Component } from 'react'
import TweenMax from 'gsap'

export const SidebarContext = React.createContext()

class SidebarProvider extends Component {
  state = {
    filterOpened: false,
    fillIcons: [],
    strokeIcons: []
  }

  render() {
    return (
      <SidebarContext.Provider
        value={{
          state: this.state,
          filterOpened: () => {
            this.setState({
              filterOpened: true
            })
          },
          addFillIcons: (filledIcons, filledStrokes) => {
            if (this.state.fillIcons.indexOf(filledIcons) < 0) { 
              this.state.fillIcons = [...this.state.fillIcons, filledIcons] 
              this.state.strokeIcons = [...this.state.strokeIcons, filledStrokes] 
              TweenMax.to(`${filledIcons}`, 0, {
                fill: 'white'
              })
              TweenMax.to(`${filledStrokes}`, 0, {
                stroke: 'white'
              })
            } else {
              const arr = this.state.fillIcons.indexOf(filledIcons)
              const arr2 = this.state.strokeIcons.indexOf(filledStrokes)
              this.state.fillIcons.splice(arr, 1)
              this.state.strokeIcons.splice(arr2, 1)

              var cols = document.getElementsByClassName(`${filledIcons}`);
              cols.fill = 'red';

              TweenMax.to(`${filledIcons}`, 0, {
                fill: '#7FA5D0'
              })
              TweenMax.to(`${filledStrokes}`, 0, {
                stroke: '#7FA5D0'
              })
            }
          },
          addStrokeIcons: filledIcons => {
            if (this.state.fillIcons.indexOf(filledIcons) < 0) {
              this.setState({ fillIcons: [...this.state.fillIcons, filledIcons] })
            }
          }
        }}
      >
        {this.props.children}
      </SidebarContext.Provider>
    )
  }
}

export default SidebarProvider
