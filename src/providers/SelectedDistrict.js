import React, { Component } from 'react'

export const LocationContext = React.createContext()

class SelectRegion extends Component {
  state = {
    selectedCity: '',
    selectedCityId: '',
    selectedDistrict: '',
    selectedDistrictId: '',
    districtParentId: '',
    firstChange: ''
  }

  render() {
    return (
      <LocationContext.Provider
        value={{
          state: this.state,
          selectedCity: (city, cityId, firstChange) => this.setState({ selectedCity: city, selectedCityId: cityId, firstChange }),
          selectedDistrict: (district, districtId, districtParentId,) => this.setState({ selectedDistrict: district, selectedDistrictId: districtId, selectedDistrictParentId: districtParentId}),
        }}
      >
        {this.props.children}
      </LocationContext.Provider>
    )
  }
}

export default SelectRegion
