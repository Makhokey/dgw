import React, { Component } from 'react'

export const AgeContext = React.createContext()

class AgeGate extends Component {
  state = {
    age: ''
  }

  render() {
    return (
      <AgeContext.Provider
        value={{
          state: this.state,
          setAge: age => this.setState({ age })
        }}
      >
        {this.props.children}
      </AgeContext.Provider>
    )
  }
}

export default AgeGate
